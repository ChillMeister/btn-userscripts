'use strict';
class MediaInfo {
    constructor(log) {
        var regex_section = /^((?:general|video|audio|text|menu)\s*(?:\#(\d+)?)*)$/i;
        var lines = log.split(/\r\n|\n|\r/);
        var header = undefined;
        for(var i = 0; i < lines.length; i++) {
            console.log(lines[i]);
            if(lines[i].length <= 1) {
                header = undefined;
                continue;
            }
            lines[i] = lines[i].trim();
            var match = lines[i].match(regex_section);
            if(match) {
                header = match[1];
                this[match[1]] = {};
            } else if(header) {
                var sp = lines[i].replace(/\s*:\s*/, '\x01').split('\x01');
                sp[0] = (sp[0] || '').trim();
                sp[1] = (sp[1] || '').trim();
                this[header][sp[0]] = sp[1];
            }
        }
        console.log(this);

        if(!this['General']) throw 'Invalid/no MediaInfo.';
    }

    static getAllMediaInfos(log) {
        var logs = log.split(/General[^\S\n]*(?:\#\d+?)*\r?\n/g).slice(1);
        var mediaInfos = [];
        for(var i = 0; i < logs.length; i++) {
            mediaInfos.push(new MediaInfo('General\n'+logs[i]));
        }
        if(mediaInfos.length == 0) {
            throw 'No valid MediaInfos found.';
        }
        console.log(mediaInfos);
        return mediaInfos;
    }

    getFilename() {
        if(!this['General']['Complete name']) throw 'No filename provided in MediaInfo.';
        return this['General']['Complete name'].match(/(?:.*\/)?(.*)/)[1];
    }

    getContainer() {
        if(!this['General']['Complete name']) throw 'No filename provided in MediaInfo.';
        var container = this['General']['Complete name'].match(/(?:.*\/)?.*\.(.*)/)[1].toUpperCase();
        if(container == 'IFO') return 'VOB';
        return container;
    }

    getVideoCodec() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var codec = video['Format'];
        var writingLibrary = video['Writing library'];
        var writingApplication = this['General']['Writing application'];
        var bitDepth = video['Bit depth'];
        var formatVersion = video['Format version'];
        var codecId = video['Codec ID'];
        var codecIdHint = video['Codec ID/Hint'];
        if(codec == 'AVC') {
            if(writingLibrary) {
                if(writingLibrary.indexOf('x264') > -1) return (bitDepth && bitDepth == '10 bits' ? 'x264-Hi10P' : 'x264');
                else if(/Zencoder Video Encoding System/i.test(writingLibrary)) return 'x264';
            }
            if(writingApplication) {
                if(/MH ENCODER/i.test(writingApplication)) return 'x264';
            }
            return 'H.264';
        } else if (codec == 'HEVC' || codec == 'hvc1' || codec == 'hev1') {
            return (writingLibrary && writingLibrary.indexOf('x265') > -1 ? 'x265' : 'H.265');
        } else if (codec == 'MPEG-4 Visual') {
            if(writingLibrary) {
                if(/XviD/i.test(writingLibrary)) return 'Xvid';
                else if(/DivX/i.test(writingLibrary)) return 'DivX';
            }
            if(codecId) {
                if(/XviD/i.test(codecId)) return 'Xvid';
                else if(/DivX|DX50/i.test(codecId)) return 'DivX';
            }
            if(codecIdHint) {
                if(/XviD/i.test(codecIdHint)) return 'Xvid';
                else if(/DivX/i.test(codecIdHint)) return 'DivX';
            }
        } else if (codec == 'MPEG Video') {
            if(formatVersion) {
                if(formatVersion == 'Version 1') return 'MPEG';
                else if(formatVersion == 'Version 2') return 'MPEG2';
            }
        } else if (codec == 'VC-1') {
            return 'VC-1';
        } else if (codec == 'VP9') {
            return 'VP9'
        }
        throw 'Unknown/unsupported video codec.';
    }

    getVideoResolution() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var height = video['Height'];
        height = parseInt(height.split(' ').join('').match(/([0-9]+pixels)/)[1]);
        var scanType = this.isInterlaced();
        if(height > 1080) {
            return '2160p';
        } else if(height > 720) {
            return (scanType == 'Interlaced' ? '1080i' : '1080p');
        } else if(height > 576) {
            return '720p';
        } else if(height == 576) {
            return (scanType == 'Interlaced' ? '576i' : '576p');
        } else {
            return 'SD';
        }
    }

    isInterlaced() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var scanType = video['Scan type'];
        var storeMethod = video['Scan type, store method'];
        if (scanType == 'Interlaced' || scanType == 'MBAFF' || storeMethod == 'Interleaved fields') return 'Interlaced';
        return 'Progressive';
    }

    getVideoStandard() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        return this['Video']['Standard'];
    }

    getNumberOfAudioTracks() {
        var keys = Object.keys(this);
        var num = 0;
        for(var i = 0; i < keys.length; i++) {
            if(keys[i].indexOf('Audio') > -1) num++;
        }
        return num;
    }

    getPrimaryAudioCodec() {
        if(!this['Audio'] && !this['Audio #1']) throw 'No audio section in MediaInfo.';
        var audio = this['Audio'] || this['Audio #1'];
        var codec = audio['Format'];
        var profile = audio['Format profile'];
        var channels = (audio['Channel(s)_Original'] || audio['Channel(s)'] || audio['Channel count']).match(/([0-9]) channels?/);
        if(channels) channels = parseInt(channels[1]);
        else channels = 0;

        if(codec == 'MPEG Audio') {
            if(profile && profile == 'Layer 3') return 'MP3';
            else if(profile && profile == 'Layer 2') return 'MP2';
        }

        var channelMap = {
            1: '1.0',
            2: '2.0',
            3: '2.1',
            6: '5.1',
            8: '7.1'
        };

        if(channelMap[channels]) {
            if(codec.indexOf('TrueHD') > -1) {
                return 'TrueHD' + channelMap[channels];
            } else if(codec == 'PCM') {
                return 'LPCM' + channelMap[channels];
            } else if(codec == 'FLAC') {
                return 'FLAC' + channelMap[channels];
            } else if(codec == 'DTS') {
                if(profile && profile == 'MA / Core') return 'DTS-HD.MA' + channelMap[channels];
                else return 'DTS' + channelMap[channels];
            } else if(codec == 'E-AC-3') {
                return 'DDP' + channelMap[channels];
            } else if(codec == 'AC-3') {
                return 'DD' + channelMap[channels];
            } else if(codec == 'AAC') {
                return 'AAC' + channelMap[channels];
            } else if(codec == 'Opus') {
                return 'Opus' + channelMap[channels];
            }
        }
        throw 'Unknown/unsupported audio codec.';
    }

    getNumberOfSubTracks() {
        var keys = Object.keys(this);
        var num = 0;
        for(var i = 0; i < keys.length; i++) {
            if(keys[i].indexOf('Text') > -1) num++;
        }
        return num;
    }
}

module.exports = MediaInfo;

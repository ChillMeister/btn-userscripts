'use strict';
const window = unsafeWindow;

function isFirefox() {
    return (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
}

const Preferences = require('./Preferences.js');

if (!isFirefox() && /user\.php\?id=[0-9]+/.test(window.location.href) && Preferences.getInstance('enable')
        .getItem('blockUserStamps', 'yes') == 'yes') {
    console.log('Blocking stamps!');
    const stampSelector = '#section3 > div > div.pad.center > img, #section3 > div > div.pad.center > a > img';
    const stampObserver = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            [].forEach.call(mutation.addedNodes, function (node) {
                if (node.nodeType != 1) {
                    return;
                }
                if (!node.matches(stampSelector)) {
                    return;
                }
                node.src = '';
            });
        });
    });
    stampObserver.observe(document, {subtree: true, childList: true});
}

const TVDB = require('./TVDBv2.js');
const TVMaze = require('./TVMaze.js');
const MediaInfo = require('./MediaInfo.js');
const Latinise = require('./latinise.js');
const Utils = require('./Utils.js');
const Promise = require('bluebird');

(function ($) {
    'use strict';
    $(document).ready(function () {
        console.log('Using jQuery version: ' + $.fn.jquery);

        alertify.parent(document.body);
        alertify.logPosition("top right");
        alertify.maxLogItems(3).delay(30000).closeLogOnClick(true);
        const url = window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, '').replace(/#/g, '');

        const usernameSelector = $('a.username');

        const myUsername = usernameSelector[0].innerHTML;
        const myUserId = usernameSelector.attr('href').match(/id=([0-9]+)/)[1];

        const enablePrefs = Preferences.getInstance('enable');

        const devPrefs = Preferences.getInstance('development');
        const isDevMode = (devPrefs.getItem('devMode', 'no') === 'yes');

        const defaultSearchProviders = {
            'srrDB':      'http://www.srrdb.com/release/details/__RELEASE_NAME__',
            'PreDB.org':  'http://predb.org/search?searchstr=__RELEASE_NAME__',
            'PreDB.me':   'http://predb.me/?search=__RELEASE_NAME__',
            'CorruptNet': 'https://pre.corrupt-net.org/index.php?q=__RELEASE_NAME__',
            'PFMonkey':   'https://pfmonkey.com/search/__RELEASE_NAME__',
            'Google':     'https://www.google.com/webhp?ie=UTF-8#q=__RELEASE_NAME__'
        };
        let searchProviders;
        try {
            searchProviders = JSON.parse(enablePrefs.getItem('searchProviders'));
        } catch (e) {
            searchProviders = defaultSearchProviders;
        }

        if (isDevMode) {
            console.log('Development mode enabled!');
        }

        function run(func, key) {
            if (enablePrefs.getItem(key, 'yes') == 'yes') {
                let args = [];
                for (let i = 2; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                return func.apply(this, args);
            }
        }

        function selectDropdown(selector, option) {
            let dd = document.querySelector(selector);
            for (let i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === option) {
                    dd.selectedIndex = i;
                    return;
                }
            }
            for (let i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === '---') {
                    dd.selectedIndex = i;
                    return;
                }
            }
        }

        function copyToClipboard(text) {

            // Create a "hidden" input
            let aux = document.createElement("input");

            // Assign it the value of the specified element
            aux.setAttribute("value", text);

            // Append it to the body
            document.body.appendChild(aux);

            // Highlight its content
            aux.select();

            // Copy the highlighted text
            let success = document.execCommand("copy");

            // Remove it from the body
            document.body.removeChild(aux);
            if (success) {
                alertify.success('Copied "' + text + '" to clipboard.');
            } else {
                alertify.error('Failed to copy to clipboard.');
            }
            return success;
        }

        function errorHandler(errorText, err) {
            errorText = errorText.replace(/\r\n|\r|\n/g, '<br>');
            if (err) {
                if (err.stack) {
                    let stackStr = Utils.escapeHTML(err.stack).replace(/\r\n|\r|\n/, '<br>');
                    stackStr = stackStr.replace(/\s{3,4}(.*)(?:\r\n|\r|\n)?/g, '<div class="tab2">$1</div><br>');
                    errorText += ': <br><div class="tab1">' + stackStr + '</div>';
                    console.error(err.stack);
                } else if (err.toString() != '[object Object]') {
                    let thrownErrText = err.toString();
                    thrownErrText = thrownErrText.replace(/\t(.*)/g, '<div class="tab2">$1</div>');
                    thrownErrText = thrownErrText.replace(/\r\n|\r|\n/g, '<br>');
                    thrownErrText = thrownErrText.replace(/(.*?)(<br>|$)/g, '<div class="tab1">$1</div>$2');
                    errorText += ': <br>' + thrownErrText + '</div>';
                    console.error(err);
                }
            }
            alertify.error(errorText);
        }

        function addPolyfillsAndStyles() {

            function injectScript(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectScriptText(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectStylesheet(link) {
                $('<link rel="stylesheet" href="' + link + '" type="text/css">').appendTo($('head'));
            }

            function injectStylesheetText(text, id) {
                $('<style type="text/css" id=' + id + '>' + text + '</style>').appendTo($('head'));
            }

            GM_addStyle(GM_getResourceText('dialog_polyfill'));

            if (isDevMode) {
                crossOriginGet('http://localhost:8080/css/bootstrap-iso.css').then(function (data) {
                    GM_addStyle(data);
                });
                crossOriginGet('http://localhost:8080/css/styles.css').then(function (data) {
                    GM_addStyle(data);
                });
            } else {
                GM_addStyle(GM_getResourceText('bootstrap_iso'));
                GM_addStyle(GM_getResourceText('userscript_styles'));
            }
        }

        function tvdbLogin() {
            const pref = Preferences.getInstance('TVDB');
            const apikey = pref.getItem('apikey');
            const username = pref.getItem('username');
            const userkey = pref.getItem('userkey');
            if (!apikey || !username || !userkey) {
                alertify.error('<div><h3>TVDB API Key Required</h3><p>Register for an API key on TVDB and fill in the appropriate fields in the Options menu.</p><p>Click to open the TVDB API key registration page.</p></div>', function (e) {
                    e.preventDefault();
                    window.open('http://thetvdb.com/?tab=apiregister', '_blank');
                    window.focus();
                });
                return false;
            } else {
                return {apikey: apikey, username: username, userkey: userkey};
            }
        }

        function checkForUpdates() {
            if (isDevMode) {
                return;
            }
            Promise.resolve($.getJSON('https://bitbucket.org/ChillMeister/btn-userscripts/raw/master/package.json'))
                .then(function (data) {
                    const scriptVersion = GM_info.script.version;
                    const prefs = Preferences.getInstance('update');
                    console.log('Script version is ' + scriptVersion + ', version in repository is ' + data.version);
                    if (scriptVersion != data.version && prefs.getItem('lastUpdateVersion') != data.version) {
                        alertify.log('<div><h3>Userscript update available</h3><p>Version ' + data.version + ' is now available (current version is ' + scriptVersion + '). Click for details.</p>', 
                            function (e) {
                                e.preventDefault();
                                window.open('https://broadcasthe.net/forums.php?action=viewthread&threadid=21126&page=1#post1103693', '_blank');
                                window.focus();
                            });
                    }
                });
        }

        function addOptionsToMenu() {
            $('#nav_index').find('> ul').append($('<li>', {
                class: 'potato-menu-item'
            }).append($('<a>', {
                href:  '#',
                text:  (isDevMode ? 'Disable Dev Mode' : 'Enable Dev Mode'),
                click: function () {
                    devPrefs.setItem('devMode', (isDevMode ? 'no' : 'yes'));
                    location.reload();
                    return false;
                }
            })));

            let dialogPage;
            if (isDevMode) {
                dialogPage = crossOriginGet('http://localhost:8080/html/options.html');
            } else {
                dialogPage = Promise.resolve(GM_getResourceText('options_dialog'));
            }
            dialogPage.then(function (data) {
                const snatchlistPrefs = Preferences.getInstance('Snatchlist');
                const tvdbPrefs = Preferences.getInstance('TVDB');

                const dialog = document.createElement('dialog');
                dialog.id = 'preferences_dialog';
                dialogPolyfill.registerDialog(dialog);
                dialog.innerHTML = data;
                $('body').append(dialog);
                $('#preferences_save').on('click', function () {
                    const savedPrefs = {};
                    $('.pref-checkbox').each(function () {
                        savedPrefs[this.value] = (this.checked ? 'yes' : 'no');
                    });
                    savedPrefs['searchProviders'] = $('#torrent-search-json').val();
                    enablePrefs.setItems(savedPrefs);

                    snatchlistPrefs.setItems({
                        'authkey':      $('#btnAuthkey').val(),
                        'torrent_pass': $('#btnPasskey').val()
                    });

                    tvdbPrefs.setItems({
                        'apikey':   $('#tvdbAPIkey').val(),
                        'username': $('#tvdbUsername').val(),
                        'userkey':  $('#tvdbUserkey').val()
                    });

                    dialog.close();
                    location.reload();
                });
                $('#preferences_cancel').on('click', function () {
                    dialog.close();
                });
                const menu = $('<li>', {
                    id:    'nav_options',
                    class: 'potato-menu-item potato-menu-has-vertical'
                });
                menu.append($('<a>', {
                    href:  '#',
                    text:  'Options',
                    click: function () {
                        dialog.showModal();
                        return false;
                    }
                }));
                $('#nav_user').after(menu);

                const leftCol = $('#search-left');
                const rightCol = $('#search-right');
                const providerNames = Object.keys(searchProviders);
                for (let i = 0; i < providerNames.length; i++) {
                    const searchName = providerNames[i];
                    const template = '<div class="checkbox"><label><input type="checkbox" class="pref-checkbox" value="search__SEARCH_NAME__">__SEARCH_NAME__</label></div>'.replace(/__SEARCH_NAME__/g, searchName);
                    if (i % 2 == 0) {
                        rightCol.append($(template));
                    } else {
                        leftCol.append($(template));
                    }
                }

                let torrentSearchJson = enablePrefs.getItem('searchProviders');
                if (!torrentSearchJson) {
                    try {
                        JSON.parse(torrentSearchJson);
                    } catch (e) {
                        torrentSearchJson = JSON.stringify(defaultSearchProviders, null, '\t');
                    }
                }
                $('#torrent-search-json').val(torrentSearchJson);

                $('#btnAuthkey').val(snatchlistPrefs.getItem('authkey', ''));
                $('#btnPasskey').val(snatchlistPrefs.getItem('torrent_pass', ''));

                $('#tvdbAPIkey').val(tvdbPrefs.getItem('apikey', ''));
                $('#tvdbUsername').val(tvdbPrefs.getItem('username', ''));
                $('#tvdbUserkey').val(tvdbPrefs.getItem('userkey', ''));

                $('.pref-checkbox').each(function () {
                    const checked = enablePrefs.getItem(this.value, 'yes');
                    this.checked = (checked == 'yes');
                });
            })
        }

        function crossOriginGet(url) {
            return new Promise(function (fulfill, reject) {
                try {
                    GM_xmlhttpRequest({
                        method:             'GET',
                        url:                url,
                        onreadystatechange: function (response) {
                            if (response.readyState != 4) {
                                return;
                            }
                            fulfill(response.responseText);
                        }
                    });
                } catch (e) {
                    console.error(e);
                    reject(e);
                }
            });
        }

        function modifyForumPage() {
            function addPMToForums() {
                $('#forums').find('table tr.colhead_dark').each(function () {
                    const id = $("td strong a", this).attr('href').replace('user.php?id=', '');
                    $("td > span + span", this).prepend('<a href="inbox.php?action=compose&to=' + id + '">[PM]</a>');
                });
            }

            function forumBBcodeGenerator() {
                // Add a text field to insert a staff note in forums
                let textbox = $('#quickpost');
                if (!textbox) {
                    return;
                }
                const container = document.createElement('div');
                const notebox = $('<textarea>', {
                    cols:  '90',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                run(function () {
                    $('<input>', {
                        type:  'button',
                        class: 'staff-note',
                        value: 'Insert FLS staff note',
                        style: 'vertical-align:middle;',
                        click: function () {
                            const oldText = textbox.val();
                            let note = notebox.val();
                            if (!note.endsWith('//' + myUsername)) {
                                note += ' //' + myUsername;
                            }
                            textbox.val(oldText + '[color=#990099]' + note + '[/color]');
                            notebox.val('');
                        }
                    }).appendTo(container);
                }, 'forumFLSBBcodeGenerator');
                run(function () {
                    $('<input>', {
                        type:  'button',
                        class: 'staff-note',
                        value: 'Insert tech staff note',
                        style: 'vertical-align:middle;',
                        click: function () {
                            const oldText = textbox.val();
                            let note = notebox.val();
                            if (!note.endsWith('//' + myUsername)) {
                                note += ' //' + myUsername;
                            }
                            textbox.val(oldText + '[[n]b][[n]color=red][[n]size=5]' + note + '[/size][/color][/b]');
                            notebox.val('');
                        }
                    }).appendTo(container);
                }, 'forumTVTechBBcodeGenerator');
                if (container.querySelector('.staff-note')) {
                    textbox.after(container);
                    notebox.before(document.createElement('br'));
                }
            }

            run(addPMToForums, 'addPMToForums');
            forumBBcodeGenerator();
        }

        function modifySeriesEditPage() {
            function noBannerLinks() {
                // I hate copy pasting the links
                const banner = $('div > form > table:nth-child(3) > tbody > tr:nth-child(2) > td.tdleft')[0];
                const fanart = $('div > form > table:nth-child(3) > tbody > tr:nth-child(6) > td.tdleft')[0];
                const poster = $('div > form > table:nth-child(3) > tbody > tr:nth-child(8) > td.tdleft')[0];

                $('<input>', {
                    type:  'button',
                    value: 'No banner available',
                    style: 'vertical-align:top;',
                    click: function () {
                        banner.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                    }
                }).appendTo(banner);
                $('<input>', {
                    type:  'button',
                    value: 'No fan art available',
                    style: 'vertical-align:top;',
                    click: function () {
                        fanart.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                    }
                }).appendTo(fanart);
                $('<input>', {
                    type:  'button',
                    value: 'No poster available',
                    style: 'vertical-align:top;',
                    click: function () {
                        poster.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                    }
                }).appendTo(poster);
            }

            function autofillSeriesPage() {
                const row = $('#content')
                    .find('div.center.thin > div:nth-child(2) > div > form > table:nth-child(4) > tbody > tr:nth-child(8) > td');
                $('<input>', {
                    type:  'button',
                    value: 'Autofill',
                    click: function () {
                        let loginInfo = tvdbLogin();
                        if (!loginInfo) {
                            return false;
                        }

                        const apikey = loginInfo['apikey'];
                        const username = loginInfo['username'];
                        const userkey = loginInfo['userkey'];

                        const tvdbClient = new TVDB();
                        const tvmazeClient = new TVMaze();

                        const seriesId = parseInt(url.match(/series\.php\?action=edit_info&seriesid=([0-9]+)/)[1]);
                        const pageContent = $('#content');

                        let tvdbSeriesId = parseInt(pageContent
                            .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                            .val());
                        let tvdbSeriesLink = pageContent
                            .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                            .val();
                        let languageId = 7;
                        if (tvdbSeriesLink) {
                            const tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                            if (tvdbLinkMatch) {
                                tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                if (tvdbLinkMatch[2]) {
                                    languageId = parseInt(tvdbLinkMatch[2]);
                                }
                            }
                        }

                        if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                            alertify.error('Series autofill requires a TVDB series URL or ID!');
                            return;
                        }

                        alertify.log('Querying TVDB and TVMaze for series ID ' + tvdbSeriesId + ' using language ID ' + languageId);

                        const tvdbPromise = tvdbClient.login(apikey, username, userkey).then(function () {
                            // poster and fanart may fail
                            return Promise.all([
                                tvdbClient.getSeries(tvdbSeriesId, languageId),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'poster', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get poster:');
                                        console.log(e);
                                        return '';
                                    }),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'fanart', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get fanart:');
                                        console.log(e);
                                        return '';
                                    })
                            ]).then(function (data) {
                                data[0]['poster'] = data[1];
                                data[0]['fanart'] = data[2];
                                return data[0];
                            });
                        });

                        const tvmazePromise = tvmazeClient.getSeriesByTVDBId(tvdbSeriesId).then(function (data) {
                            return tvmazeClient.getSeasons(data['id']).then(function (seasons) {
                                data['seasons'] = seasons;
                                return data;
                            });
                        }).catch(function () {
                            return {};
                        });

                        Promise.all([tvdbPromise, tvmazePromise]).then(function (data) {
                            const tvdbData = data[0];
                            const tvmazeData = data[1];

                            const form = pageContent.find('div.center.thin > div:nth-child(2) > div > form');
                            // first table elements go straight down
                            const firstTable = form.find('table:nth-child(3) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['seriesName']) {
                                firstTable[0].value = tvdbData['seriesName'];
                            } else {
                                throw 'Series name is empty on TVDB; please fix the language ID.';
                            }
                            if (tvdbData['banner']) {
                                firstTable[1].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['banner'];
                            } else if (!firstTable[1].value) {
                                firstTable[1].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                            }
                            if (tvdbData['imdbId']) {
                                firstTable[2].value = 'http://imdb.com/title/' + tvdbData['imdbId'];
                            }
                            if (!tvdbSeriesLink || languageId == 7) {
                                firstTable[3].value = 'http://thetvdb.com/?tab=series&id=' + tvdbSeriesId + '&lid=7';
                            }
                            //firstTable[4].value = tvrage;
                            if (tvdbData['fanart']) {
                                firstTable[5].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['fanart'];
                            } else if (!firstTable[5].value) {
                                firstTable[5].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                            }
                            //firstTable[6].value = youtube;
                            if (tvdbData['poster']) {
                                firstTable[7].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['poster'];
                            } else if (!firstTable[7].value) {
                                firstTable[7].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                            }

                            // second table elements go across then down
                            const secondTable = form.find('table:nth-child(4) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['airsDayOfWeek']) {
                                secondTable[0].value = tvdbData['airsDayOfWeek'];
                            }
                            if (tvdbData['airsTime']) {
                                secondTable[1].value = tvdbData['airsTime'];
                            }
                            if (tvdbData['rating']) {
                                secondTable[2].value = tvdbData['rating'];
                            }
                            if (tvmazeData['network'] && tvmazeData['network']['country'] && tvmazeData['network']['country']['code']) {
                                secondTable[3].value = tvmazeData['network']['country']['code'];
                            }
                            if (tvdbData['firstAired']) {
                                secondTable[4].value = tvdbData['firstAired'];
                            }
                            if (tvdbData['network']) {
                                secondTable[5].value = tvdbData['network'];
                            }
                            if (tvdbData['runtime']) {
                                secondTable[6].value = tvdbData['runtime'];
                            }
                            if (tvdbData['siteRating']) {
                                secondTable[7].value = tvdbData['siteRating'];
                            }
                            if (tvmazeData['seasons']) {
                                secondTable[8].value = tvmazeData['seasons'].length;
                            }
                            if (tvdbData['status']) {
                                secondTable[9].value = tvdbData['status'];
                            }
                            if (tvmazeData['type']) {
                                secondTable[10].value = tvmazeData['type'];
                            }
                            secondTable[11].value = tvdbSeriesId;
                            if (tvmazeData['externals'] && tvmazeData['externals']['tvrage']) {
                                secondTable[12].value = tvmazeData['externals']['tvrage'];
                            }
                            return tvdbData['overview'];
                        }).then(function (overview) {
                            if (overview) {
                                alertify.log('Adding series overview for series ID ' + tvdbSeriesId);
                                return Promise.resolve($.post('https://broadcasthe.net/series.php', {
                                    seriesid: seriesId,
                                    action:   'edit_summary',
                                    btn_body: overview
                                }));
                            } else {
                                alertify.error('No overview available on TVDB for series ID ' + tvdbSeriesId);
                                return false;
                            }
                        }).then(function () {
                            alertify.success('Autofill for series ID ' + tvdbSeriesId + ' complete.');
                        }).catch(function (err) {
                            errorHandler('Autofill for series ID ' + tvdbSeriesId + ' failed', err);
                        });

                        return false;
                    }
                }).prependTo(row);
            }

            run(noBannerLinks, 'noBannerLinks');
            run(autofillSeriesPage, 'autofillSeriesPage');
        }

        function modifyTorrentEditPage() {
            let currentOrigin;

            function internalModifier() {
                if (!currentOrigin) {
                    const l = document.getElementById('origin');
                    currentOrigin = l.options[l.selectedIndex].value;
                }
                window.Origin = exportFunction(function () {
                    const l = document.getElementById('origin');
                    const origin = l.options[l.selectedIndex].value;
                    let outputStr = '';
                    if (origin === 'Mixed') {
                        outputStr = 'You can\'t upload a season pack with mixed origin without prior staff consent!';
                    } else if (origin === 'Internal') {
                        outputStr = 'Multiplier set to 2.0x.';
                    }

                    if (origin !== window.currentOrigin && origin === 'Internal') {
                        $('#internal').prop('checked', true);
                        const torrentId = url.match(/(?!torrent)id=([0-9]+)/)[1];
                        $.post('https://broadcasthe.net/torrents.php', {
                            action:    'takemulti',
                            torrentid: torrentId,
                            upload:    2
                        });
                        $('select[name="upload"]').val('2');
                    }
                    document.getElementById('mixedOriginWarning').innerHTML = outputStr;
                    currentOrigin = origin;
                }, window);
            }

            run(internalModifier, 'internalModifier');
            modifyTorrentUploadPage();
        }

        function modifyTorrentUploadPage() {
            function torrentBBcodeGenerator() {
                // Same thing but for torrents
                const tech_specs_table = $('#table_manual_upload_2').find('tbody');
                const textbox = $('#release_desc');
                const row = document.createElement('tr');
                let label = $('<td>', {
                    class: 'label',
                    text:  'Add Staff Note'
                }).appendTo(row);
                const container = document.createElement('td');
                row.appendChild(container);
                const notebox = $('<textarea>', {
                    cols:  '60',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val('[b][color=red][size=5]' + note + '[/size][/color][/b]\n\n' + oldText);
                        notebox.val('');
                    }
                }).appendTo(container);
                tech_specs_table.append(row);
            }

            function blockContainerChange() {
                // Prevent autofill from changing MP4 -> SD or MKV -> 720p
                window.Format = exportFunction(function () {
                    console.log('Blocked format/resolution auto change');
                }, window);
            }

            function autoTorrentFix() {
                $('#post').before($('<input>', {
                    id:    'autofix',
                    type:  'button',
                    value: 'Auto fix torrent',
                    click: function () {
                        try {
                            const containerSelect = document.querySelector('#format');
                            const codecSelect = document.querySelector('#bitrate');
                            const sourceSelect = document.querySelector('#media');
                            const resolutionSelect = document.querySelector('#resolution');
                            const originSelect = document.querySelector('#origin');

                            const log = document.querySelector('#release_desc').value;
                            const mediaInfos = MediaInfo.getAllMediaInfos(log);
                            const mi = mediaInfos[0];

                            let container = mi.getContainer();
                            let videoCodec = mi.getVideoCodec();
                            const audioCodec = mi.getPrimaryAudioCodec();
                            let resolution = mi.getVideoResolution();
                            const origin = originSelect.options[originSelect.selectedIndex].text;
                            let source = '';

                            const releaseNameBox = document.querySelector('#scenename');
                            let releaseName = releaseNameBox.value;

                            const oldData = {};
                            oldData['Release name'] = releaseName;
                            oldData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            oldData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            oldData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            oldData['Origin'] = origin;
                            oldData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            releaseName = releaseName.replace(/^\.*/, '');
                            releaseName = releaseName.replace(/^F1/, 'Formula.1');
                            releaseName = releaseName.replace(/Prelims/i, 'Preliminaries');
                            releaseName = releaseName.replace(/\(|\)|:|SD/g, '.');
                            releaseName = releaseName.replace(/&/g, '.and.');
                            releaseName = releaseName.replace(/BDRip|Bluray/, 'BluRay');
                            releaseName = releaseName.replace(/\.s([0-9]+)e([0-9]+)\./, ".S$1E$2.");
                            releaseName = Latinise.latinise(releaseName);

                            const releaseNameRes = releaseName.match(/\.([0-9]+)(p|i)\./);
                            if (releaseNameRes) {
                                if (resolution == 'SD') {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], '');
                                } else {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], resolution);
                                }
                            }

                            const sourceCasing = {
                                'HDTV':    /hdtv/i,
                                'PDTV':    /pdtv/i,
                                'DSR':     /dsr/i,
                                'DVDRip':  /dvdrip/i,
                                'TVRip':   /tvrip/i,
                                'VHSRip':  /vhsrip/i,
                                'BluRay':  /bluray/i,
                                'BDRip':   /bdrip/i,
                                'DVD5':    /dvd5/i,
                                'DVD9':    /dvd9/i,
                                'WEBRip':  /webrip/i,
                                'WEB-DL':  /web.?dl/i,
                                'WEB':     /web/i,
                                'BD25':    /bd25/i,
                                'BD50':    /bd50/i,
                                'Mixed':   /mixed/i,
                                'Unknown': /unknown/i,
                                'Remux':   /remux/i,
                            };

                            const sourceCases = Object.keys(sourceCasing);
                            for (let i = 0; i < sourceCases.length; i++) {
                                releaseName = releaseName.replace(sourceCasing[sourceCases[i]], sourceCases[i]);
                            }

                            releaseName = releaseName.replace(/\\|'|"/g, '');
                            releaseName = releaseName.replace(/\s|\.-\./g, '.');
                            releaseName = releaseName.replace(/\.{2,}/g, '.');

                            const releaseRegex = /(.*)(HDTV|PDTV|DSR|DVDRip|TVRip|VHSRip|BluRay\.Remux|BluRay|BDRip|WEBRip|WEB-DL|WEB|BD25|BD50|Mixed|Unknown)(?:\.(.*))?\.(.*?(?:-Hi10P)?)(?:[-.]|$)(.*)/i;
                            const releaseMatch = releaseName.match(releaseRegex);

                            let tvTitle = document.querySelector('#title').value;
                            tvTitle = tvTitle.replace(/\\/g, '');
                            document.querySelector('#title').value = tvTitle;

                            // Don't modify scene release names or release names from P2P that are Japanese (likely to be anime) unless in a season pack
                            const shouldModifyReleaseName = ((origin != "Scene" && !(origin == "P2P" && document.querySelector('#country').selectedIndex == 52)) || /^Season [0-9]+$/.test(tvTitle));
                            
                            if (releaseMatch) {
                                source = releaseMatch[2];
                                if (source == 'WEB') {
                                    source = 'WEBRip';
                                } else if (source == 'BDRip') {
                                    source = 'BluRay';
                                }
                                // Check if scene
                                if (shouldModifyReleaseName) {
                                    const group = releaseMatch[5] || 'NOGRP';
                                    let beginningOfReleaseName = releaseMatch[1];
                                    if (resolution != 'SD' && beginningOfReleaseName.indexOf(resolution) == -1) {
                                        beginningOfReleaseName += resolution + '.';
                                    }

                                    releaseName = beginningOfReleaseName + source + '.';

                                    if (audioCodec != 'MP3' && audioCodec != 'MP2') {
                                        releaseName += audioCodec + '.';
                                    }

                                    releaseName += videoCodec + '-' + group;
                                    releaseNameBox.value = releaseName;
                                }
                            }
                            if (resolution == '576p') {
                                resolution = 'SD';
                            }
                            if (videoCodec == 'x264') {
                                videoCodec = 'H.264';
                            } else if (videoCodec == 'DivX') {
                                videoCodec = 'DiVX';
                            } else if (videoCodec == 'Xvid') {
                                videoCodec = 'XViD';
                            } else if (videoCodec == 'x265') {
                                videoCodec = 'H.265';
                            } else if (container == 'VOB') {
                                videoCodec = 'DVD';
                            } else if (oldData['container'] == 'ISO') {
                                container = 'ISO';
                            }

                            if (source.includes('BluRay') && container != 'M2TS') {
                                source = 'Bluray';
                            }

                            selectDropdown('#format', container);
                            selectDropdown('#bitrate', videoCodec);
                            selectDropdown('#media', source);
                            selectDropdown('#resolution', resolution);

                            const newData = {};
                            newData['Release name'] = releaseNameBox.value;
                            newData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            newData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            newData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            newData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            const differences = [];
                            const differenceKeys = Object.keys(newData);

                            for (let i = 0; i < differenceKeys.length; i++) {
                                const key = differenceKeys[i];
                                const newValue = newData[key];
                                const oldValue = oldData[key];
                                if (oldValue != newValue) {
                                    if (key == 'Release name') {
                                        const diffTexts = Utils.getHTMLDiff(oldValue, newValue);
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + diffTexts[0] + '</div><br><div class="tab2">' + diffTexts[1] + '</div></div>');
                                    } else {
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + oldData[key] + ' &#8594; ' + newData[key] + '</div></div>');
                                    }
                                }
                            }
                            if (differences.length > 0) {
                                alertify.success('Changed the following items:<br>' + differences.join('<br>'));
                            } else {
                                alertify.error('No changes made.');
                            }
                            return false;
                        } catch (err) {
                            errorHandler('Failed to automatically fix torrent', err);
                            return false;
                        }
                    }
                }));
            }

            function checkReleaseNameLength() {
                const releaseNameBox = $('#scenename');
                const lengthWarning = $('<div>', {
                    style: 'color: red; font-weight: bold'
                });
                lengthWarning.hide();
                releaseNameBox.after(lengthWarning);
                releaseNameBox.on('input', function (e) {
                    if (e.target.value.length > 100) {
                        lengthWarning.text('Warning: Release name is ' + e.target.value.length + ' characters and will be truncated to 100.');
                        lengthWarning.show();
                    } else {
                        lengthWarning.hide();
                    }
                });
            }

            run(checkReleaseNameLength, 'checkReleaseNameLength');
            run(autoTorrentFix, 'autoTorrentFix');
            run(torrentBBcodeGenerator, 'torrentBBcodeGenerator');
            run(blockContainerChange, 'blockContainerChange');
        }

        function modifyTorrentDetailPage() {
            function autofillTorrentGroupDescription() {
                function generateTorrentDescription(episodeName, season, episode, firstAired, overview, episodeImage) {
                    let desc = '[b]Episode Name:[/b] ' + (episodeName || '');
                    desc += '\n[b]Season:[/b] ' + season;
                    desc += '\n[b]Episode:[/b] ' + episode;
                    desc += '\n[b]Aired:[/b] ' + firstAired || '';
                    desc += '\n\n[b]Episode Overview:[/b] ';
                    if (overview) {
                        desc += '\n' + overview;
                    }
                    if (episodeImage) {
                        desc += '\n\n[b]Episode Image[/b] \n[img=https://cdn2.broadcasthe.net/tvdb/banners/' + episodeImage + ']';
                    }
                    return desc;
                }


                const linkbox = $('div.linkbox')[0];
                $('<a>', {
                    text:  '[Autofill description]',
                    href:  '#',
                    click: function () {
                        if (/^BroadcasTheNet$/.test(document.title)) {
                            alertify.error('Fix this torrent before autofilling!');
                            return false;
                        }
                        const groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                        let episodeName = document.querySelector('#content > div > h2 > a > img').title || '';
                        let episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                        let doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                        let seasonPackNum = episodeName.match(/.*Season ([0-9]+).*/);
                        let episodePromptPromise;
                        if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                            episodePromptPromise = alertify.okBtn('OK')
                                .cancelBtn('Cancel')
                                .defaultValue('S00E01')
                                .prompt('Could not find a season/episode in the episode name. Please provide one manually.')
                                .then(function (resolvedValue) {
                                    resolvedValue.event.preventDefault();
                                    if (resolvedValue.buttonClicked == 'ok') {
                                        episodeName = resolvedValue.inputValue || '';
                                        episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                                        doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                                        seasonPackNum = episodeName.match(/.*Season ([0-9]+).*/);
                                    }
                                    if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                                        throw 'Autofill only supports numbered episodes or season packs.';
                                    }
                                });
                        } else {
                            episodePromptPromise = Promise.resolve();
                        }

                        episodePromptPromise.then(function () {
                            let torrentSeason = -1;
                            let torrentEpisode = -1;
                            let torrentSecondEpisode = -1;
                            if (doubleEpisodeSE) {
                                doubleEpisodeSE = doubleEpisodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = doubleEpisodeSE[1];
                                torrentEpisode = doubleEpisodeSE[2];
                                torrentSecondEpisode = doubleEpisodeSE[3];
                            } else if (seasonPackNum) {
                                seasonPackNum = parseInt(seasonPackNum[1]);
                                torrentSeason = seasonPackNum;
                            } else {
                                episodeSE = episodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = episodeSE[1];
                                torrentEpisode = episodeSE[2];
                            }

                            let loginInfo = tvdbLogin();
                            if (!loginInfo) {
                                return false;
                            }

                            const apikey = loginInfo.apikey;
                            const username = loginInfo.username;
                            const userkey = loginInfo.userkey;

                            const client = new TVDB();

                            const getYearRegex = /([0-9]+)\-[0-9]+\-[0-9]+/;

                            const btnSeriesId = document.querySelector('#content > div > h2 > a').href.split('?id=')[1];
                            let tvdbSeriesId = 0;
                            let languageId = 7;
                            let p = Promise.resolve($.get('https://broadcasthe.net/series.php?action=edit_info&seriesid=' + btnSeriesId))
                                .then(function (data) {
                                    const pageContent = $(data).find('#content');
                                    tvdbSeriesId = parseInt(pageContent
                                        .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                                        .val());
                                    const tvdbSeriesLink = pageContent
                                        .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                                        .val();
                                    if (tvdbSeriesLink) {
                                        const tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                                        if (tvdbLinkMatch) {
                                            tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                            if (tvdbLinkMatch[2]) {
                                                languageId = parseInt(tvdbLinkMatch[2]);
                                            }
                                        }
                                    }

                                    if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                                        throw 'Cannot autofill - series does not have a TVDB ID.';
                                    }

                                    alertify.log('Torrent autofill is currently working...');

                                    return tvdbSeriesId;
                                });

                            if (torrentSecondEpisode !== -1) {
                                // double episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentSecondEpisode)
                                            ]);
                                        }).then(function (data) {
                                            let desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            desc += '\n\n' + generateTorrentDescription(data[2]['episodeName'], torrentSeason, torrentSecondEpisode, data[2]['firstAired'], data[2]['overview'], data[2]['filename']);
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else if (torrentEpisode !== -1) {
                                // episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode)
                                            ]);
                                        }).then(function (data) {
                                            const desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else {
                                // season pack
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesSeason(tvdbSeriesId, torrentSeason)
                                            ]);
                                        }).then(function (data) {
                                            data[1].sort(function (e1, e2) {
                                                return e1['airedEpisodeNumber'] - e2['airedEpisodeNumber'];
                                            });
                                            const desc = data[1].map(function (episode) {
                                                return generateTorrentDescription(episode['episodeName'], episode['airedSeason'], episode['airedEpisodeNumber'], episode['firstAired'], episode['overview'], episode['filename']);
                                            }).reduce(function (previousValue, currentValue) {
                                                return previousValue + currentValue + '\n\n';
                                            }, '');
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1][0]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1][0]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            }
                            return p.then(function (data) {
                                return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data[0]))
                                    .then(function () {
                                        return data[1];
                                    });
                            }).then(function (data) {
                                if (data) {
                                    return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data));
                                } else {
                                    alertify.error('Could not autofill aired year since it was unavailable on TVDB.');
                                    return true;
                                }
                            }).then(function () {
                                return Promise.resolve($.get(url));
                            }).then(function (data) {
                                const dom = $(data);
                                $('div.body:eq(1)').replaceWith(dom.find('div.body:eq(1)'));
                                $('#content').find('> div > h2').replaceWith(dom.find('#content > div > h2'));
                                alertify.success('Torrent autofill complete - changes are now visible.');
                            });
                        }).catch(function (err) {
                            errorHandler('Autofill for torrent group ' + groupId + ' failed', err);
                        });
                        return false;
                    }
                }).prependTo(linkbox);
            }

            run(autofillTorrentGroupDescription, 'autofillTorrentGroup');

            $('tr.pad > td > div.linkbox').each(function () {
                // Hide obnoxious file list
                const $this = $(this);
                const filelist = $this.next().next();
                $('<a>', {
                    text:  '[File List]',
                    href:  '#',
                    click: function () {
                        filelist.toggle();
                        return false;
                    }
                }).prependTo($this);

                // Count number of files in torrents
                const filenum = filelist.find('tbody').children().length - 1;
                let output = ' (' + filenum + ' file';
                if (filenum > 1) {
                    output += 's';
                }
                output += ')';
                $this.parent().parent().prev().prev().children()[1].innerHTML += output;

                if (filenum > 50) {
                    filelist.hide();
                }
            });

            // Add search links for torrents
            $('table.torrent_table > tbody > tr > td:nth-child(1) > a').each(function () {
                const releaseNode = this.parentNode.parentNode.nextSibling.nextSibling.children[0];
                const relQueryObj = $(releaseNode.children[0]);
                run(function () {
                    relQueryObj.append($('<br>'));
                }, 'torrentSearchesNewLine');

                // Only add log button if this user has log access
                if ($('input[placeholder=\'Log\']').length > 0) {
                    run(function (that) {
                        const linkbox = that.previousSibling.previousSibling.children;
                        const reqLink = linkbox[linkbox.length - 1].href.split('=');
                        const torrentId = reqLink[reqLink.length - 1];
                        relQueryObj.append($('<a>', {
                            href: 'https://broadcasthe.net/log.php?search=' + torrentId,
                            text: '[Log] '
                        }));
                    }, 'searchLog', this);
                }

                if (this.innerHTML.endsWith('Scene')) {
                    const releaseName = Utils.cleanReleaseName(releaseNode.childNodes[0].nodeValue);

                    const providerNames = Object.keys(searchProviders);

                    for (let i = 0; i < providerNames.length; i++) {
                        const searchName = providerNames[i];
                        const searchUrl = searchProviders[searchName];
                        run(function (searchUrl, searchName, releaseName) {
                            let newSearchUrl = searchUrl.replace(/__RELEASE_NAME__/, releaseName);
                            newSearchUrl = newSearchUrl.replace(/__ENCODED_RELEASE_NAME__/, encodeURIComponent(releaseName));
                            relQueryObj.append($('<a>', {
                                href: newSearchUrl,
                                text: '[' + searchName + '] '
                            }));
                        }, 'search' + searchName, searchUrl, searchName, releaseName);
                    }
                }
            });

            // Add reseed button to all torrents
            run(function () {
                $('td > blockquote').prev().filter(function () {
                    const prevId = $(this).attr('id');
                    return prevId && prevId.indexOf('subtitle') > -1;
                }).each(function () {
                    const $this = $(this);
                    const torrentId = $this.attr('id').match('subtitle_dialog_([0-9]+)')[1];
                    const groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                    $this.after($('<div id="linkbox" style="text-align: center"><form method="post" action=""><input type="hidden" id="action" name="action" value="reseed"><input type="hidden" id="gid" name="gid" value="' + groupId + '"><input type="hidden" id="tid" name="tid" value="' + torrentId + '"><input type="submit" id="reseed" name="reseed" value="Request a reseed"></form></div>'));
                });
            }, 'addReseedButton');

            // Copy release link to clipboard
            $('a[title="RequestLink"]').on('click', function(event) {
                event.stopPropagation();
                copyToClipboard(this.href);
                return false;
            });
        }

        function modifyRequestPage() {
            function requestBBcodeGenerator() {
                // Same thing but for requests
                const submit_button = $('tr').last();
                const textbox = $('textarea[name="description"]');
                const row = document.createElement('tr');
                let label = $('<td>', {
                    class: 'label',
                    text:  'Add Staff Note'
                }).appendTo(row);
                const container = document.createElement('td');
                row.appendChild(container);
                const notebox = $('<textarea>', {
                    cols:  '74',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val('[b][color=red][size=5]' + note + '[/size][/color][/b]\n\n' + oldText);
                        notebox.val('');
                    }
                }).appendTo(container);
                submit_button.before(row);
            }

            run(requestBBcodeGenerator, 'requestBBcodeGenerator');
        }

        function addPMToUsernames() {
            $('#user .box.pad.center tbody tr > td a,' +
                '#torrents .torrent_table table ~ div + blockquote > a,' +
                '#torrents .torrent_table table ~ table + blockquote > a,' +
                '#torrents .torrent_table .torrent > td:nth-child(3) > .nobr > a')
                .each(function () {
                    const $this = $(this);
                    const id = $this.attr('href').replace('user.php?id=', '');
                    $this.after(' <a href="inbox.php?action=compose&to=' + id + '">[PM]</a> ');
                });
        }

        function modifyMassDeletePage() {
            // cache series data when mass deleting
            let seriesData;

            function expandMassDeleteReleaseNames() {
                $('#content').find('> div.thin.center > table > tbody > tr > td:nth-child(5)').each(function () {
                    const $this = $(this);
                    $this.addClass('mass-delete-releasename');
                    const span = $this.find('span');
                    const releaseName = span.attr('title');
                    span.remove();
                    $this.text(releaseName);
                });
            }

            function seasonPackMassDelete() {

                const row = document.createElement('tr');
                const container = document.createElement('td');
                container.setAttribute('colspan', '7');
                row.appendChild(container);
                $('<span>', {
                    class: 'label',
                    text:  'URL of season pack to autoclean: ',
                    style: 'vertical-align:middle;'
                }).appendTo(container);
                const urlbox = $('<input>', {
                    id:    'season_pack_url',
                    style: 'vertical-align:middle;'
                });
                urlbox.attr('size', 60);
                urlbox.appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Autoclean Season Pack',
                    style: 'vertical-align:middle;margin-left:5px',
                    click: function () {
                        const seasonPackUrl = urlbox.val();
                        const seriesId = parseInt(url.match(/seriesid=([0-9]+)&auth=/)[1]);
                        const seasonTorrentId = parseInt(seasonPackUrl.split('action=reqlink&id=')[1]);
                        if (isNaN(seasonTorrentId)) {
                            alertify.error('Not a valid link.');
                            urlbox.val('');
                            return false;
                        }

                        alertify.log('Trumping in progress...');

                        let p;
                        if (seriesData) {
                            console.log('Using cached data');
                            p = Promise.resolve(seriesData);
                        } else {
                            p = Promise.resolve($.get('https://broadcasthe.net/series.php?id=' + seriesId))
                                .then(function (data) {
                                    const dom = $(data);
                                    return $.makeArray(dom.find('#discog_table > tbody > tr > td.group.discog > div > strong > a.episode'))
                                        .filter(function (link) {
                                            return link.text !== 'S01E01' && link.text !== 'S01E01E02' && link.text !== 'S01E00';
                                        }).map(function (link) {
                                            return link.href;
                                        });
                                }).then(function (links) {
                                    alertify.log('Fetching torrent data. Please wait...');
                                    console.log('Grabbing info for ' + links.length + ' torrents');
                                    return Promise.all(links.map(function (link) {
                                        return Promise.resolve($.get(link))
                                            .reflect()
                                            .then(function (inspection) {
                                                if (inspection.isFulfilled()) {
                                                    console.log('Grabbed ' + link);
                                                    return inspection.value();
                                                } else {
                                                    console.error('Grabbing ' + link + ' failed: ' + inspection.reason());
                                                    throw 'Grabbing ' + link + ' failed: ' + inspection.reason();
                                                }
                                            }).catch(function () {
                                                return Promise.resolve($.get(link))
                                                    .reflect()
                                                    .then(function (inspection) {
                                                        if (inspection.isFulfilled()) {
                                                            console.log('Grabbed ' + link);
                                                            return inspection.value();
                                                        } else {
                                                            console.error('Grabbing ' + link + ' failed: ' + inspection.reason());
                                                            throw 'Grabbing ' + link + ' failed: ' + inspection.reason();
                                                        }
                                                    })
                                            });
                                    }));
                                }).then(function (data) {
                                    seriesData = data.reduce(function (previousValue, currentValue) {
                                        const dom = $(currentValue);
                                        dom.find('tr.pad > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(1)')
                                            .each(function (index, elem) {
                                                const enclosingTable = elem.parentNode.parentNode.parentNode.parentNode.parentNode;
                                                const torrentId = parseInt($(enclosingTable)
                                                    .attr('id')
                                                    .replace('torrent_', ''));
                                                previousValue[elem.innerHTML] = {
                                                    torrentId:   parseInt(torrentId),
                                                    releaseName: Utils.cleanReleaseName(enclosingTable.previousSibling.previousSibling.children[0].childNodes[0].nodeValue)
                                                };
                                            });
                                        return previousValue;
                                    }, {});
                                    return seriesData;
                                });
                        }
                        p.then(function (torrentData) {
                            return Promise.resolve($.get(seasonPackUrl))
                                .then(function (data) {
                                    const torrentGroupId = parseInt(data.match(/torrents\.php\?action=editgroup&(?:amp;)?groupid=([0-9]+)/)[1]);
                                    const dom = $(data).find('#torrent_' + seasonTorrentId);
                                    const filenames = $.makeArray(dom.find('td > table:nth-child(3) > tbody > tr')
                                        .not('.colhead_dark'))
                                        .map(function (row) {
                                            return row.querySelector('td').innerHTML;
                                        });
                                    const releaseName = Utils.cleanReleaseName(dom.prev()[0].children[0].childNodes[0].nodeValue);
                                    return {releaseName: releaseName, fileNames: filenames, groupId: torrentGroupId};
                                }).then(function (data) {
                                    return Promise.resolve($.get('https://broadcasthe.net/torrents.php?action=editgroup&groupid=' + data['groupId']))
                                        .then(function (groupData) {
                                            const dom = $(groupData);
                                            const torrentCategory = dom.find('#content > div.center.thin > div:nth-child(4) > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > select > option:nth-child(2)');
                                            if (!torrentCategory.attr('selected')) {
                                                throw 'The provided link was not for a season.\n<a href="' + seasonPackUrl + '">' + seasonPackUrl + '</a>';
                                            }
                                            const torrentGroupName = dom.find('#content > div.center.thin > div:nth-child(8) > form > div > input[type="text"]:nth-child(4)')
                                                .attr('value');
                                            const seasonNum = parseInt(torrentGroupName.match(/Season ([0-9]+)/)[1]);
                                            let isSpecial = torrentGroupName.match(/Season [0-9]+ - (.+)|.*(Extras).*|.*(Specials).*/);
                                            if (isSpecial) {
                                                isSpecial = isSpecial[1] || isSpecial[2] || isSpecial[3];
                                            }
                                            data['seasonNum'] = seasonNum;
                                            data['isSpecial'] = isSpecial;
                                            return data;
                                        });
                                }).then(function (data) {
                                    const seasonNum = data['seasonNum'];
                                    const packFileNames = data['fileNames'];
                                    let isSpecial = data['isSpecial'];
                                    const torrentFileNamesArr = Object.keys(torrentData);
                                    const foundFileNames = torrentFileNamesArr.filter(function (fileName) {
                                        return packFileNames.indexOf(fileName) > -1;
                                    });
                                    const numDirfixesInPack = packFileNames.filter(function (fileName) {
                                        return /.*?\.DIRFIX\..*/.test(fileName);
                                    }).length;
                                    const numDirfixesFound = torrentFileNamesArr.filter(function (fileName) {
                                        return /.*?\.DIRFIX\..*/.test(torrentData[fileName]['releaseName']);
                                    }).length;
                                    if (foundFileNames.length == 0 && numDirfixesFound == 0 && numDirfixesInPack == 0) {
                                        throw 'No matching single episodes found.';
                                    } else if ((seasonNum > 1 && foundFileNames.length != packFileNames.length) || (!isSpecial && seasonNum == 1 && foundFileNames.length != packFileNames.length - 1)) {
                                        let confirmMessage = 'Warning: I found ' + foundFileNames.length + ' matching single episodes but there are ' + packFileNames.length + ' in the pack.';
                                        if (numDirfixesInPack > 0) {
                                            confirmMessage += ' However, there are ' + numDirfixesInPack + ' DIRFIXed single episodes in the pack (these will have to be deleted manually).';
                                        }
                                        confirmMessage += ' Delete anyway?';
                                        return alertify.okBtn('Yes, delete anyway')
                                            .cancelBtn('No, don\'t delete')
                                            .confirm(confirmMessage)
                                            .then(function (resolvedValue) {
                                                resolvedValue.event.preventDefault();
                                                console.log(resolvedValue);
                                                if (resolvedValue.buttonClicked == 'ok') {
                                                    return foundFileNames;
                                                } else {
                                                    throw 'Number of single episodes found differs from number of files in pack.';
                                                }
                                            })
                                    }
                                    return foundFileNames;
                                });
                        }).then(function (selectedFileNames) {
                            let trumpData = selectedFileNames.map(function (fileName) {
                                console.log('Trumping torrent ' + seriesData[fileName]['torrentId'] + ': ' + fileName);
                                return '&' + encodeURIComponent('torrentid[]') + '=' + seriesData[fileName]['torrentId'];
                            }).reduce(function (previousValue, currentValue) {
                                return previousValue + currentValue;
                            }, 'action=trump');
                            trumpData += '&reason=' + encodeURIComponent('Season Pack Release') + '&extra=' + encodeURIComponent(seasonPackUrl + ' //' + myUsername);
                            return alertify.okBtn('Yes, delete')
                                .cancelBtn('No, don\'t delete')
                                .confirm('Going to delete ' + selectedFileNames.length + ' torrents. Continue?')
                                .then(function (resolvedValue) {
                                    resolvedValue.event.preventDefault();
                                    if (resolvedValue.buttonClicked == 'ok') {
                                        return Promise.resolve($.post('https://broadcasthe.net/series.php', trumpData))
                                            .then(function () {
                                                return Promise.resolve($.get(url));
                                            }).then(function (data) {
                                                const dom = $(data);
                                                $('table').replaceWith(dom.find('table'));
                                                run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
                                                run(seasonPackMassDelete, 'seasonPackMassDelete');
                                                return selectedFileNames.length;
                                            });
                                    } else {
                                        throw 'Cancelled by user.';
                                    }
                                });
                        }).then(function (num) {
                            alertify.success('' + num + ' single episodes trumped.');
                        }).catch(function (err) {
                            errorHandler('Error trumping single episodes', err);
                        });
                        return false;
                    }
                }).appendTo(container);
                $('tr').last().after(row);

                $('#content').find('> div.thin.center > table > tbody > tr:nth-child(8) > td > input[type="submit"]')
                    .attr('onclick', 'return false')
                    .on('click', function () {
                        const checked = document.querySelectorAll('input[name="torrentid[]"]:checked');
                        let trumpData = Array.prototype.slice.call(checked).map(function (checkbox) {
                            return '&' + encodeURIComponent('torrentid[]') + '=' + checkbox.value;
                        }).reduce(function (previousValue, currentValue) {
                            return previousValue + currentValue;
                        }, 'action=trump');
                        const reasonSelector = document.querySelector('#content > div.thin.center > table > tbody > tr:nth-child(7) > td > select');
                        trumpData += '&reason=' + encodeURIComponent(reasonSelector.options[reasonSelector.selectedIndex].value) + '&extra=' + encodeURIComponent($('#content').find('> div.thin.center > table > tbody > tr:nth-child(7) > td > input')
                                .val());
                        alertify.okBtn('Yes').cancelBtn('No').confirm('Are you sure?').then(function (resolvedValue) {
                            resolvedValue.event.preventDefault();
                            if (resolvedValue.buttonClicked == 'ok') {
                                return Promise.resolve($.post('https://broadcasthe.net/series.php', trumpData))
                                    .then(function () {
                                        return Promise.resolve($.get(url));
                                    }).then(function (data) {
                                        const dom = $(data);
                                        $('table').replaceWith(dom.find('table'));
                                        run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
                                        run(seasonPackMassDelete, 'seasonPackMassDelete');
                                        return true;
                                    });
                            }
                        });
                        return false;
                    });
            }

            run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
            run(seasonPackMassDelete, 'seasonPackMassDelete');
        }

        function modifySnatchlistPage() {
            function snatchListFilter() {
                // pulled from https://github.com/Userscriptz/BTN-Snatchlist/blob/master/main.user.js

                function changeTable() {
                    const pref = Preferences.getInstance('Snatchlist');
                    const snatchView = pref.getItem('selectedmode', 'all');
                    const links = pref.getItem('dllinks', 'no');

                    if (links == 'yes') {
                        $('.download-btn').show();
                    } else {
                        $('.download-btn').hide();
                    }

                    $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                        if ($(this).attr('class') != 'colhead_dark') {
                            const id = '#' + $(this).attr('id');
                            const status = $(id + ' > td:nth-child(7) > span').text();
                            const seedTimeLeft = $(id + ' > td:nth-child(5)').text();
                            let remove;

                            if (snatchView == 'showcomplete') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Seeding');
                            }
                            else if (snatchView == 'showcomplete2') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Complete');
                            }
                            else if (snatchView == 'seedtimeleft') {
                                remove = (seedTimeLeft == 'Complete' || status == 'Complete');
                            }
                            else if (snatchView == 'inactive') {
                                remove = (status != 'Inactive');
                            }
                            else if (snatchView == 'leeching') {
                                remove = (status != 'Leeching');
                            }
                            else {
                                remove = false;
                            }

                            if (remove) {
                                $(id).hide();
                            } else {
                                $(id).show();
                            }
                        }
                    });
                }

                const pref = Preferences.getInstance('Snatchlist');
                const selectedMode = pref.getItem('selectedmode', 'all');
                const dllinks = pref.getItem('dllinks', 'no');

                let cssStyle = '.mode-active {color:limegreen; font-weight:bold;} ';
                cssStyle += '#div-1 {position:relative; padding-top:40px;} ';
                cssStyle += '#div-1a {position:absolute; top:0; right:0; width:170px;} ';
                cssStyle += '#div-1b {position:absolute; top:0; left:0; width:600px;} ';
                cssStyle += '.mode-hidden {display: none} ';
                $("<style>")
                    .prop("type", "text/css")
                    .html(cssStyle)
                    .appendTo("head");

                const selectModeLink = function (text, key, mode, active) {
                    return $('<a>', {
                        text:  text,
                        href:  '#',
                        click: function (e) {
                            pref.setItem(key, mode);
                            $('a').filter('.mode-active.' + key).removeClass('mode-active');
                            $(this).addClass('mode-active');
                            changeTable();
                            e.preventDefault();
                        },
                        class: (active ? 'mode-active ' : '') + key
                    });
                };

                const div1b = $('<div>', {
                    id:   'div-1b',
                    text: 'Torrents: '
                });

                const div1a = $('<div>', {
                    id:   'div-1a',
                    text: 'Downloads: '
                });

                div1b.append(selectModeLink('All', 'selectedmode', 'all', selectedMode == 'all'), ' | ',
                    selectModeLink('Inactive', 'selectedmode', 'inactive', selectedMode == 'inactive'), ' | ',
                    selectModeLink('Leeching', 'selectedmode', 'leeching', selectedMode == 'leeching'), ' | ',
                    selectModeLink('Seed Time Left', 'selectedmode', 'seedtimeleft', selectedMode == 'seedtimeleft'), ' | ',
                    selectModeLink('Complete (Seeding)', 'selectedmode', 'showcomplete', selectedMode == 'showcomplete'), ' | ',
                    selectModeLink('Complete', 'selectedmode', 'showcomplete2', selectedMode == 'showcomplete2'));

                div1a.append(selectModeLink('Enable', 'dllinks', 'yes', dllinks == 'yes'), ' | ',
                    selectModeLink('Disable', 'dllinks', 'no', dllinks == 'no'));

                const div1 = $('<div>', {
                    id: 'div-1'
                });
                div1.append(div1b, div1a);
                div1.prependTo('#content');

                const modifySnatchlistElements = function () {
                    let authkey = pref.getItem('authkey');
                    let torrent_pass = pref.getItem('torrent_pass');
                    if (dllinks == 'yes' && (!authkey || !torrent_pass)) {
                        alertify.error('<div><h3>Need torrent passkey</h3><p>To generate torrent download links, your passkey is needed. It can be entered in the Options menu.</p><p>Optionally, you can also disable torrent download links to hide this notice.</p></div>');
                    } else {
                        $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                            if ($(this).attr('class') != 'colhead_dark') {
                                const id = '#' + $(this).attr('id');
                                const torrentid = $(id + ' > td:nth-child(1) > span > a:nth-child(2)')
                                    .attr('href')
                                    .match(/torrentid=([0-9]+)/)[1];
                                const link = $('<a>', {
                                    href:  'http://broadcasthe.net/torrents.php?action=download&id=' + torrentid + '&authkey=' + authkey + '&torrent_pass=' + torrent_pass,
                                    class: 'download-btn'
                                });
                                $('<img>', {
                                    src: 'https://cdn.broadcasthe.net/common/browsetags/download.png'
                                }).appendTo(link);
                                link.appendTo(id + ' > td:nth-child(1) > span');
                            }
                        });
                    }
                    changeTable();
                };

                window.SortSnatch = exportFunction(function (sort, uid, page) {
                    let old = '';
                    let hnr = '';
                    if (window.location.href.search("old") !== -1) {
                        old = '&old=1';
                    }
                    if (window.location.href.search("hnr") !== -1) {
                        hnr = '&hnr=1';
                    }
                    console.log('Fetching page ' + page);
                    Promise.resolve($.get("https://broadcasthe.net/snatchlist.php?type=ajax&id=" + uid + "&sort=" + sort + "&page=" + page + old + hnr))
                        .then(function (data) {
                            data = $(data)[0];
                            console.log('Fetched page ' + page);
                            $('#SnatchData').replaceWith(data);
                            modifySnatchlistElements();
                        });
                    return false;
                }, window);

                modifySnatchlistElements();
            }

            snatchListFilter();
        }

        function modifyTorrentListPage() {

            function getWatchedSeries() {
                const postUrl = 'https://broadcasthe.net/forums.php?action=get_post&post=1095026';
                return Promise.resolve($.get(postUrl))
                    .then(function (data) {
                        return data.split('\n').slice(1).map(function (value) {
                            value = value.trim();
                            const ret = {};
                            const matched = value.match(/\[url=https:\/\/broadcasthe.net\/series.php\?id=([0-9]+)](.*?)\[\/url](.*)/);
                            if (matched) {
                                ret.url = 'https://broadcasthe.net/series.php?id=' + matched[1];
                                ret.name = matched[2];
                                if (matched[3].length > 0) {
                                    ret.reason = matched[3].replace(' - ', '');
                                } else {
                                    ret.reason = 'No reason specified';
                                }
                            } else {
                                ret.url = '';
                                if (value.indexOf(' - ') > -1) {
                                    const splits = value.split(' - ', 2);
                                    ret.name = splits[0];
                                    ret.reason = splits[1];
                                } else {
                                    ret.name = value;
                                    ret.reason = 'No reason specified';
                                }
                            }
                            return ret;
                        });
                    });
            }

            function getWatchedUsers() {
                const postUrl = 'https://broadcasthe.net/forums.php?action=get_post&post=1095024';
                return Promise.resolve($.get(postUrl))
                    .then(function (data) {
                        return data.split('\n').slice(1).map(function (value) {
                            value = value.trim();
                            const ret = {};
                            if (value.indexOf(' - ') > -1) {
                                const splits = value.split(' - ', 2);
                                ret.name = splits[0];
                                ret.reason = splits[1];
                            } else {
                                ret.name = value;
                                ret.reason = 'No reason specified';
                            }
                            return ret;
                        });
                    });
            }

            run(function () {
                $('#torrent_table').find('tbody > tr > td:nth-child(3)').slice(1).each(function () {
                    const $this = $(this);

                    // cleanup some crap
                    $this.find('br').remove();
                    $this.children('img').remove();
                    let contents = $this.contents();
                    if ($this.children('a')[0].title == 'View Series') {
                        contents.slice(3, 6).wrapAll('<div class="nobr">');
                    } else {
                        $this.children('a').slice(0, 1).wrapAll('<div class="nobr">');
                    }

                    const torrentId = $this.find('a[title="View Torrent"]')
                        .attr('href')
                        .match(/torrents\.php\?id=[0-9]+&torrentid=([0-9]+)/)[1];

                    contents = $this.contents();

                    // get rid of <strong> tag wrapping "Internal" if it exists
                    const torrentSpecsNode = contents.slice(5, 6);
                    let nextNode = torrentSpecsNode[0].nextSibling;
                    let torrentSpecsText = torrentSpecsNode[0].nodeValue;
                    while (nextNode.localName != 'div') {
                        torrentSpecsText += nextNode.innerHTML || nextNode.nodeValue;
                        nextNode = nextNode.nextSibling;
                        $(nextNode.previousSibling).remove();
                    }

                    // color code format, codec, source, resolution, origin, year
                    const torrentSpecs = torrentSpecsText.match(/\[([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+)]\s*\[([0-9]+)]/);
                    let newTorrentSpecsNode = '<div class="nobr">[';
                    for (let i = 1; i < 5; i++) {
                        newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[i] + '">' + torrentSpecs[i];
                        newTorrentSpecsNode += '</div> / ';
                    }
                    newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[5] + '">' + torrentSpecs[5];
                    newTorrentSpecsNode += '</div>] [<div class="inline year">' + torrentSpecs[6] + '</div>] [<a href="https://broadcasthe.net/torrents.php?action=edit&id=' + torrentId + '">Edit</a>]</div>';
                    torrentSpecsNode.replaceWith(newTorrentSpecsNode);
                });
            }, 'colorCodeTorrents');
            run(function () {
                // Disabled temporarily
                const tableSelector = $('#torrent_table');
                if (tableSelector.width() > 1000 && false) {
                    tableSelector.find('tbody > tr > td:nth-child(3) > div > span').each(function () {
                        const that = $(this);
                        that.text(' ' + that.attr('title'));
                    });
                } else {
                    tableSelector.find('tbody > tr > td:nth-child(3) > div > span').mouseover(function () {
                        const that = $(this);
                        const newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    }).mouseout(function () {
                        const that = $(this);
                        const newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    });
                }
            }, 'expandReleaseNames');
            const p1 = run(function () {
                getWatchedSeries().then(function (seriesList) {
                    const seriesLinks = $('#torrent_table')
                        .find('tbody > tr > td:nth-child(3) > div:nth-child(2) > a:nth-child(1)');
                    seriesList.forEach(function (series) {
                        seriesLinks.filter(function () {
                            const that = $(this);
                            return that.attr('href') === series.url || that.text() === series.name;
                        }).after('<div class="watched inline" title="' + series.reason + '"> [!]</div>');
                    });
                });
            }, 'getWatchedSeries');
            const p2 = run(function () {
                getWatchedUsers().then(function (userList) {
                    const userLinks = $('#torrent_table')
                        .find('tbody > tr > td:nth-child(3) > div:nth-child(4) > a:nth-child(2)');
                    userList.forEach(function (user) {
                        userLinks.filter(function () {
                            return $(this).text() === user.name;
                        }).after('<div class="watched inline" title="' + user.reason + '"> [!]</div>');
                    });
                });
            }, 'getWatchedUsers');
            Promise.all([p1, p2]).then(function () {
                $('.watched').mouseover(function () {
                    const that = $(this);
                    if (that.attr('title').length < 50) {
                        that.text(' ' + that.attr('title'));
                    }
                }).mouseout(function () {
                    $(this).text(' [!]');
                });
            });
        }

        function modifyReportsPage() {

            function refreshReportsPage() {
                return Promise.resolve($.get(window.location.href)).then(function (data) {
                    const dom = window.jQuery(data);
                    dom.find('span[class=date-utc]').map(function (i, e) {
                        const LocalDate = new Date($(e).attr('x-jqdate') * 1000);
                        $(e).html(LocalDate.toString());
                    });
                    $('#content').replaceWith(dom.find('#content'));
                    window.jQuery('span.expandable')
                        .expander({slicePoint: 35, expandText: '[show]', userCollapseText: '[hide]'});
                    modifyReportsPage();
                    //run(addLinksToReports, 'addLinksToReports');
                })
            }

            function addLinksToReports() {

                function getReport(id) {
                    return Promise.resolve($.post('https://broadcasthe.net/reports.php', {
                        id:     id,
                        action: 'view_report'
                    }));
                }

                // Highlight reports with the current date (UTC) in them
                if (url.indexOf('status=Resolved') == -1) {
                    const date = new Date();
                    $('span.expandable').filter(function () {
                        let matched = this.innerHTML.match(/([0-9]{4}).([0-9]{1,2}).([0-9]{1,2})/);
                        if (!matched) {
                            return false;
                        }
                        const cleanDate = new Date(Date.UTC(parseInt(matched[1]), parseInt(matched[2]) - 1, parseInt(matched[3]), 0, 0, 0, 0));
                        return date > cleanDate;
                    }).each(function (index, value) {
                        const $this = $(value);
                        const row = $this.closest('tr');
                        row.css('background-color', '#500050');
                        const viewLink = row.find('td:nth-child(7) > a');
                        if (viewLink.length === 1) {
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve cleaned',
                                text:  '[Cleaned]'
                            }));
                            viewLink.after($('<br>'));
                        }
                    });
                    $('#content').find('> table > tbody > tr > td:nth-child(2) > a').filter(function () {
                        return /log\.php\?search=[0-9]+/.test(this.href);
                    }).each(function (index, value) {
                        const $this = $(value);
                        const row = $this.closest('tr');
                        row.css('background-color', '#500050');
                        const viewLink = row.find('td:nth-child(7) > a');
                        if (viewLink.length === 1) {
                            // Add links to those
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve deleted',
                                text:  '[Deleted]'
                            }));
                            viewLink.after($('<br>'));
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve cleaned',
                                text:  '[Cleaned]'
                            }));
                            viewLink.after($('<br>'));
                        }
                    });
                }
                $('#content').find('> table > tbody').on('click', '.autoresolve', function (e) {
                    const className = e.target.className;
                    if (className.indexOf('autoresolve') == -1) {
                        return true;
                    }
                    const $this = $(this);
                    const reportId = $this.closest('td').find('a:nth-child(1)').attr('onclick').match(/showReport\(([0-9]+)\)/)[1];
                    getReport(reportId).then(function (data) {
                        const dom = $(data);
                        const formDataPreprocessed = dom.serializeArray();
                        const formData = {};
                        for (let i = 0; i < formDataPreprocessed.length; i++) {
                            const elem = formDataPreprocessed[i];
                            formData[elem['name']] = elem['value'];
                        }
                        let comment = '';
                        if (className.indexOf('cleaned') > -1) {
                            comment = 'Cleaned\n//' + myUsername;
                        } else if (className.indexOf('deleted') > -1) {
                            comment = 'Deleted\n//' + myUsername;
                        }
                        formData['status'] = 'Resolved';
                        formData['comment'] = comment;
                        formData['userMessage'] = comment;
                        if (comment == '') {
                            throw 'Empty report comment.';
                        }
                        return alertify.okBtn('Resolve')
                            .cancelBtn('Cancel')
                            .confirm('Resolve report ' + formData['reportid'] + '?')
                            .then(function (resolvedValue) {
                                if (resolvedValue.buttonClicked == 'ok') {
                                    return Promise.resolve($.post('https://broadcasthe.net/reports.php', formData))
                                        .then(function () {
                                            alertify.success('Resolved report ' + formData['reportid'] + ':\n\t' + comment);
                                            return refreshReportsPage();
                                        });
                                } else {
                                    alertify.error('Did not resolve report ' + formData['reportid'] + '.');
                                }
                            });
                    });
                    return false;
                })
            }

            function disableReportsAutorefresh() {
                // Hook form submit if not Firefox
                if (!isFirefox()) {
                    $(document).ready(function () {
                        (function ($window) {
                            $window('#reports_dialog').dialog('destroy');
                            $window('#reports_dialog').dialog({
                                autoOpen: false,
                                modal:    true,
                                show:     'fade',
                                hide:     'fade',
                                width:    585,
                                height:   400,
                                buttons:  {
                                    Close: function () {
                                        $window('#reports_dialog').dialog('close');
                                    },
                                    Save:  function () {
                                        $window('#reports_dialog').dialog('close');
                                        const formData = $('#repoform').serialize();
                                        $.post('https://broadcasthe.net/reports.php', formData)
                                            .then(function () {
                                                return refreshReportsPage();
                                            });
                                    }
                                }
                            });
                        })(window.jQuery);
                    })
                } else {
                    console.log('Warning: hooking reports.php dialogs does not work in Firefox.');
                }
            }

            run(disableReportsAutorefresh, 'disableReportsAutorefresh');
            run(addLinksToReports, 'addLinksToReports');
        }

        function modifyStaffPMPage() {

            function staffPMBBcodeGenerator() {
                // Add a text field to insert a staff note in PMs
                // This way I don't have to !bbcode2 all the time
                let textbox = $('#quickpost');
                if (!textbox) {
                    return;
                }
                const container = document.createElement('div');
                const notebox = $('<textarea>', {
                    cols:  '70',
                    rows:  '3',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val(oldText + '[[n]b][[n]color=red][[n]size=5]' + note + '[/size][/color][/b]');
                        notebox.val('');
                    }
                }).appendTo(container);
                textbox.after(container);
                notebox.before(document.createElement('br'));
            }

            function selfAssignStaffPM() {
                const assignToMe = $('<input>', {
                    type:  'button',
                    value: 'Assign to me',
                    click: function () {
                        Promise.resolve($.post('https://broadcasthe.net/staffpm.php?action=assign', {
                            convid: url.match(/id=([0-9]+)/)[1],
                            assign: 'user_' + myUserId
                        })).then(function () {
                            location.reload();
                        });
                        return false;
                    }
                });

                $('#messageform').find('> input[type="button"]:nth-child(7)').after(assignToMe);
            }

            function autoOpenCommonAnswers() {
                $('#common_answers').show();
            }

            run(staffPMBBcodeGenerator, 'staffPMBBcodeGenerator');
            run(selfAssignStaffPM, 'selfAssignStaffPM');
            run(autoOpenCommonAnswers, 'autoOpenCommonAnswers');
        }

        function modifyUserDetailPage() {
            const userId = url.match(/user.php\?id=([0-9]+)/)[1];
            run(function () {
                $('#section2')
                    .find('li:nth-child(14)')
                    .after('<li><a href="https://broadcasthe.net/snatchlist.php?id=' + userId + '">Snatchlist</a></li>');
            }, 'addSnatchlistLink');
            run(function () {
                let stampSelector = $('#section3').find('> div > div.pad.center');
                stampSelector.find('> img, a').remove();
            }, 'blockUserStamps');
            stampObserver.disconnect();
        }

        function modifyUserProfileOptionsPage() {
            run(function () {
                $('#torpersnatch')
                    .replaceWith('<input name="torpersnatch" id="torpersnatch" type="number" min="10" max="100000" step="10" value="1000" size="6">');
                $('#torperpage')
                    .replaceWith('<input name="torperpage" id="torperpage" type="number" min="25" max="100000" step="5" value="100" size="6">');
            }, 'modifyUserProfileOptionsPage');
        }

        addPolyfillsAndStyles();
        addOptionsToMenu();

        run(checkForUpdates, 'checkForUpdates');
        run(addPMToUsernames, 'addPMToUsernames');

        if (/^user\.php\?action=edit&userid=[0-9]+/.test(url)) {
            modifyUserProfileOptionsPage();
        }
        if (/^torrents.php\?(?:page=[0-9]+&)?(?:(?:id=[0-9]+)|(?:torrentid=[0-9]+))/.test(url)) {
            modifyTorrentDetailPage();
        }
        if (/^staffpm.php\?action=viewconv/.test(url)) {
            modifyStaffPMPage();
        }
        if (/^torrents.php\?action=edit&id=[0-9]+/.test(url)) {
            modifyTorrentEditPage();
        }
        if (/^upload.php/.test(url)) {
            modifyTorrentUploadPage();
        }
        if (/^series.php\?action=edit_info/.test(url)) {
            modifySeriesEditPage();
        }
        if (/^requests.php\?action=edit/.test(url)) {
            modifyRequestPage();
        }
        if (/^series.php\?action=trump&seriesid=[0-9]+&auth=[0-9a-f]+/.test(url)) {
            modifyMassDeletePage();
        }
        if (/^snatchlist.php/.test(url)) {
            modifySnatchlistPage();
        }
        if (/^torrents.php.*(?!id)/.test(url)) {
            modifyTorrentListPage();
        }
        if (/^reports.php/.test(url)) {
            modifyReportsPage();
        }
        if (/^user.php\?id=[0-9]+/.test(url)) {
            modifyUserDetailPage();
        }
        if (/^forums.php/.test(url)) {
            modifyForumPage();
        }
    });
}($));

'use strict';
class Preferences {
    constructor() {
        if (GM_setValue !== undefined && GM_getValue !== undefined && GM_deleteValue !== undefined) {
            this.useGM = true;
            return;
        }
        // check if local storage supported
        try {
            this.useLocalStorage = (localStorage !== 'undefined');
        } catch (e) {
            // error accessing local storage (user may have blocked access)
            console.error(e);
            this.useLocalStorage = false;
        }
    }

    setItem(key, value) {
        if (this.useGM) {
            GM_setValue(key, String(value));
        } else if (this.useLocalStorage) {
            try {
                localStorage.setItem(key, value);
            } catch (e) {
                // error modifying local storage (out of space?)
                console.error(e);
            }
        }
    }

    getItem(key, defaultValue) {
        if (this.useGM) {
            return GM_getValue(key, defaultValue);
        } else if (this.useLocalStorage) {
            try {
                var result = localStorage.getItem(key);
                return (result !== null ? result : defaultValue);
            } catch (e) {
                // error reading from local storage
                console.error(e);
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    removeItem(key) {
        if (this.useGM) {
            GM_deleteValue(key);
        } else if (this.useLocalStorage) {
            try {
                localStorage.removeItem(key);
            } catch (e) {
                // error modifying local storage
                console.error(e);
            }
        }
    }

    clear() {
        if (this.useGM) {
            // todo
        }
        if (this.useLocalStorage) {
            try {
                localStorage.clear();
            } catch (e) {
                // error modifying local storage
                console.error(e);
            }
        }
    }
}

class NamedPreferences {
    constructor(namespace, preferenceManager) {
        this.manager = preferenceManager;
        this.prefix = 'nerfed-preferences/' + namespace;
    }

    setItem(key, value) {
        this.manager.setItem(this.prefix + '/' + key, value);
    }

    setItems(obj) {
        var that = this;
        Object.keys(obj).forEach(function (key) {
            that.manager.setItem(that.prefix + '/' + key, obj[key]);
        });
    }

    getItem(key, defaultValue) {
        return this.manager.getItem(this.prefix + '/' + key, defaultValue);
    }

    removeItem(key) {
        this.manager.removeItem(this.prefix + '/' + key);
    }
}

var instance = new Preferences();
var namedInstances = [];
module.exports.getInstance = function (namespace) {
    if (!namedInstances[namespace]) {
        namedInstances[namespace] = new NamedPreferences(namespace, instance);
    }
    return namedInstances[namespace];
};

'use strict';
var window = unsafeWindow;

function isFirefox() {
    return (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
}

var Preferences = require('./Preferences.js');

if (!isFirefox() && /user\.php\?id=[0-9]+/.test(window.location.href) && Preferences.getInstance('enable')
        .getItem('blockUserStamps', 'yes') == 'yes') {
    console.log('Blocking stamps!');
    var stampSelector = '#section3 > div > div.pad.center > img, #section3 > div > div.pad.center > a > img';
    var stampObserver = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            [].forEach.call(mutation.addedNodes, function (node) {
                if (node.nodeType != 1) {
                    return;
                }
                if (!node.matches(stampSelector)) {
                    return;
                }
                node.src = '';
            });
        });
    });
    stampObserver.observe(document, {subtree: true, childList: true});
}

var TVDB = require('./TVDBv2.js');
var TVMaze = require('./TVMaze.js');
var MediaInfo = require('./MediaInfo.js');
var Promise = require('bluebird');
var Latinise = require('./latinise.js');
var Utils = require('./Utils.js');

(function ($) {
    'use strict';
    $(document).ready(function () {
        console.log('Using jQuery version: ' + $.fn.jquery);

        alertify.parent(document.body);
        alertify.logPosition("top right");
        alertify.maxLogItems(3).delay(30000).closeLogOnClick(true);
        var url = window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, '').replace(/#/g, '');

        var enablePrefs = Preferences.getInstance('enable');

        var defaultSearchProviders = {
            'srrDB':      'http://www.srrdb.com/release/details/__RELEASE_NAME__',
            'PreDB.org':  'http://predb.org/search?searchstr=__RELEASE_NAME__',
            'PreDB.me':   'http://predb.me/?search=__RELEASE_NAME__',
            'CorruptNet': 'https://pre.corrupt-net.org/index.php?q=__RELEASE_NAME__',
            'PFMonkey':   'https://pfmonkey.com/search/__RELEASE_NAME__',
            'Google':     'https://www.google.com/webhp?ie=UTF-8#q=__RELEASE_NAME__'
        };
        var searchProviders;
        try {
            searchProviders = JSON.parse(enablePrefs.getItem('searchProviders'));
        } catch (e) {
            searchProviders = defaultSearchProviders;
        }

        function run(func, key) {
            if (enablePrefs.getItem(key, 'yes') == 'yes') {
                var args = [];
                for (var i = 2; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                return func.apply(this, args);
            }
        }

        function selectDropdown(selector, option) {
            var dd = document.querySelector(selector);
            for (var i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === option) {
                    dd.selectedIndex = i;
                    break;
                }
            }
        }

        function copyToClipboard(text) {

            // Create a "hidden" input
            var aux = document.createElement("input");

            // Assign it the value of the specified element
            aux.setAttribute("value", text);

            // Append it to the body
            document.body.appendChild(aux);

            // Highlight its content
            aux.select();

            // Copy the highlighted text
            var success = document.execCommand("copy");

            // Remove it from the body
            document.body.removeChild(aux);
            if (success) {
                alertify.success('Copied "' + text + '" to clipboard.');
            } else {
                alertify.error('Failed to copy to clipboard.');
            }
            return success;
        }

        function errorHandler(errorText, err) {
            errorText = errorText.replace(/\r\n|\r|\n/g, '<br>');
            if (err) {
                if (err.stack) {
                    var stackStr = Utils.escapeHTML(err.stack).replace(/\r\n|\r|\n/, '<br>');
                    stackStr = stackStr.replace(/\s{3,4}(.*)(?:\r\n|\r|\n)?/g, '<div class="tab2">$1</div><br>');
                    errorText += ': <br><div class="tab1">' + stackStr + '</div>';
                    console.error(err.stack);
                } else if (err.toString() != '[object Object]') {
                    var thrownErrText = err.toString();
                    thrownErrText = thrownErrText.replace(/\t(.*)/g, '<div class="tab2">$1</div>');
                    thrownErrText = thrownErrText.replace(/\r\n|\r|\n/g, '<br>');
                    thrownErrText = thrownErrText.replace(/(.*?)(<br>|$)/g, '<div class="tab1">$1</div>$2');
                    errorText += ': <br>' + thrownErrText + '</div>';
                    console.error(err);
                }
            }
            alertify.error(errorText);
        }

        function addPolyfillsAndStyles() {

            function injectScript(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectScriptText(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectStylesheet(link) {
                $('<link rel="stylesheet" href="' + link + '" type="text/css">').appendTo($('head'));
            }

            function injectStylesheetText(text, id) {
                $('<style type="text/css" id=' + id + '>' + text + '</style>').appendTo($('head'));
            }

            GM_addStyle(GM_getResourceText('dialog_polyfill'));
            GM_addStyle(GM_getResourceText('bootstrap_iso'));
            GM_addStyle(GM_getResourceText('userscript_styles'));
        }

        function tvdbLogin() {
            var pref = Preferences.getInstance('TVDB');
            var apikey = pref.getItem('apikey');
            var username = pref.getItem('username');
            var userkey = pref.getItem('userkey');
            if (!apikey || !username || !userkey) {
                alertify.error('<div><h3>TVDB API Key Required</h3><p>Register for an API key on TVDB and fill in the appropriate fields in the Options menu.</p><p>Click to open the TVDB API key registration page.</p></div>', function (e) {
                    e.preventDefault();
                    window.open('http://thetvdb.com/?tab=apiregister', '_blank');
                    window.focus();
                });
                return false;
            } else {
                return {apikey: apikey, username: username, userkey: userkey};
            }
        }

        function addOptionsToMenu() {
            var dialogPage = Promise.resolve(GM_getResourceText('options_dialog'));
            dialogPage.then(function (data) {
                var snatchlistPrefs = Preferences.getInstance('Snatchlist');
                var tvdbPrefs = Preferences.getInstance('TVDB');

                var dialog = document.createElement('dialog');
                dialog.id = 'preferences_dialog';
                dialogPolyfill.registerDialog(dialog);
                dialog.innerHTML = data;
                $('body').append(dialog);
                $('#preferences_save').on('click', function () {
                    var savedPrefs = {};
                    $('.pref-checkbox').each(function () {
                        savedPrefs[this.value] = (this.checked ? 'yes' : 'no');
                    });
                    savedPrefs['searchProviders'] = $('#torrent-search-json').val();
                    enablePrefs.setItems(savedPrefs);

                    snatchlistPrefs.setItems({
                        'authkey':      $('#btnAuthkey').val(),
                        'torrent_pass': $('#btnPasskey').val()
                    });

                    tvdbPrefs.setItems({
                        'apikey':   $('#tvdbAPIkey').val(),
                        'username': $('#tvdbUsername').val(),
                        'userkey':  $('#tvdbUserkey').val()
                    });

                    dialog.close();
                    location.reload();
                });
                $('#preferences_cancel').on('click', function () {
                    dialog.close();
                });
                var menu = $('<li>', {
                    id:    'nav_options',
                    class: 'potato-menu-item potato-menu-has-vertical'
                });
                menu.append($('<a>', {
                    href:  '#',
                    text:  'Options',
                    click: function () {
                        dialog.showModal();
                        return false;
                    }
                }));
                $('#nav_user').after(menu);

                var leftCol = $('#search-left');
                var rightCol = $('#search-right');
                var providerNames = Object.keys(searchProviders);
                for (var i = 0; i < providerNames.length; i++) {
                    var searchName = providerNames[i];
                    var template = '<div class="checkbox"><label><input type="checkbox" class="pref-checkbox" value="search__SEARCH_NAME__">__SEARCH_NAME__</label></div>'.replace(/__SEARCH_NAME__/g, searchName);
                    if (i % 2 == 0) {
                        rightCol.append($(template));
                    } else {
                        leftCol.append($(template));
                    }
                }

                var torrentSearchJson = enablePrefs.getItem('searchProviders');
                if (!torrentSearchJson) {
                    try {
                        JSON.parse(torrentSearchJson);
                    } catch (e) {
                        torrentSearchJson = JSON.stringify(defaultSearchProviders, null, '\t');
                    }
                }
                $('#torrent-search-json').val(torrentSearchJson);

                $('#btnAuthkey').val(snatchlistPrefs.getItem('authkey', ''));
                $('#btnPasskey').val(snatchlistPrefs.getItem('torrent_pass', ''));

                $('#tvdbAPIkey').val(tvdbPrefs.getItem('apikey', ''));
                $('#tvdbUsername').val(tvdbPrefs.getItem('username', ''));
                $('#tvdbUserkey').val(tvdbPrefs.getItem('userkey', ''));

                $('.pref-checkbox').each(function () {
                    var checked = enablePrefs.getItem(this.value, 'yes');
                    this.checked = (checked == 'yes');
                });
            })
        }

        function crossOriginGet(url) {
            return new Promise(function (fulfill, reject) {
                try {
                    GM_xmlhttpRequest({
                        method:             'GET',
                        url:                url,
                        onreadystatechange: function (response) {
                            if (response.readyState != 4) {
                                return;
                            }
                            fulfill(response.responseText);
                        }
                    });
                } catch (e) {
                    console.error(e);
                    reject(e);
                }
            });
        }

        function modifyForumPage() {
            function addPMToForums() {
                $('#forums').find('table tr.colhead_dark').each(function () {
                    var id = $("td strong a", this).attr('href').replace('user.php?id=', '');
                    $("td > span + span", this).prepend('<a href="inbox.php?action=compose&to=' + id + '">[PM]</a>');
                });
            }

            run(addPMToForums, 'addPMToForums');
        }

        function modifySeriesEditPage() {
            function noBannerLinks() {
                // I hate copy pasting the links
                var banner = $('div > form > table:nth-child(3) > tbody > tr:nth-child(2) > td.tdleft')[0];
                var fanart = $('div > form > table:nth-child(3) > tbody > tr:nth-child(6) > td.tdleft')[0];
                var poster = $('div > form > table:nth-child(3) > tbody > tr:nth-child(8) > td.tdleft')[0];

                $('<input>', {
                    type:  'button',
                    value: 'No banner available',
                    style: 'vertical-align:top;',
                    click: function () {
                        banner.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                    }
                }).appendTo(banner);
                $('<input>', {
                    type:  'button',
                    value: 'No fan art available',
                    style: 'vertical-align:top;',
                    click: function () {
                        fanart.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                    }
                }).appendTo(fanart);
                $('<input>', {
                    type:  'button',
                    value: 'No poster available',
                    style: 'vertical-align:top;',
                    click: function () {
                        poster.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                    }
                }).appendTo(poster);
            }

            function autofillSeriesPage() {
                var row = $('#content')
                    .find('div.center.thin > div:nth-child(2) > div > form > table:nth-child(4) > tbody > tr:nth-child(8) > td');
                $('<input>', {
                    type:  'button',
                    value: 'Autofill',
                    click: function () {
                        var loginInfo = tvdbLogin();
                        if (!loginInfo) {
                            return false;
                        }

                        var apikey = loginInfo['apikey'];
                        var username = loginInfo['username'];
                        var userkey = loginInfo['userkey'];

                        var tvdbClient = new TVDB();
                        var tvmazeClient = new TVMaze();

                        var seriesId = parseInt(url.match(/series\.php\?action=edit_info&seriesid=([0-9]+)/)[1]);
                        var pageContent = $('#content');

                        var tvdbSeriesId = parseInt(pageContent
                            .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                            .val());
                        var tvdbSeriesLink = pageContent
                            .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                            .val();
                        var languageId = 7;
                        if (tvdbSeriesLink) {
                            var tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                            if (tvdbLinkMatch) {
                                tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                if (tvdbLinkMatch[2]) {
                                    languageId = parseInt(tvdbLinkMatch[2]);
                                }
                            }
                        }

                        if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                            alertify.error('Series autofill requires a TVDB series URL or ID!');
                            return;
                        }

                        alertify.log('Querying TVDB and TVMaze for series ID ' + tvdbSeriesId + ' using language ID ' + languageId);

                        var tvdbPromise = tvdbClient.login(apikey, username, userkey).then(function () {
                            // poster and fanart may fail
                            return Promise.all([
                                tvdbClient.getSeries(tvdbSeriesId, languageId),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'poster', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get poster:');
                                        console.log(e);
                                        return '';
                                    }),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'fanart', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get fanart:');
                                        console.log(e);
                                        return '';
                                    })
                            ]).then(function (data) {
                                data[0]['poster'] = data[1];
                                data[0]['fanart'] = data[2];
                                return data[0];
                            });
                        });

                        var tvmazePromise = tvmazeClient.getSeriesByTVDBId(tvdbSeriesId).then(function (data) {
                            return tvmazeClient.getSeasons(data['id']).then(function (seasons) {
                                data['seasons'] = seasons;
                                return data;
                            });
                        }).catch(function () {
                            return {};
                        });

                        Promise.all([tvdbPromise, tvmazePromise]).then(function (data) {
                            var tvdbData = data[0];
                            var tvmazeData = data[1];

                            var form = pageContent.find('div.center.thin > div:nth-child(2) > div > form');
                            // first table elements go straight down
                            var firstTable = form.find('table:nth-child(3) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['seriesName']) {
                                firstTable[0].value = tvdbData['seriesName'];
                            } else {
                                throw 'Series name is empty on TVDB; please fix the language ID.';
                            }
                            if (tvdbData['banner']) {
                                firstTable[1].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['banner'];
                            } else if (!firstTable[1].value) {
                                firstTable[1].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                            }
                            if (tvdbData['imdbId']) {
                                firstTable[2].value = 'http://imdb.com/title/' + tvdbData['imdbId'];
                            }
                            if (!tvdbSeriesLink || languageId == 7) {
                                firstTable[3].value = 'http://thetvdb.com/?tab=series&id=' + tvdbSeriesId + '&lid=7';
                            }
                            //firstTable[4].value = tvrage;
                            if (tvdbData['fanart']) {
                                firstTable[5].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['fanart'];
                            } else if (!firstTable[5].value) {
                                firstTable[5].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                            }
                            //firstTable[6].value = youtube;
                            if (tvdbData['poster']) {
                                firstTable[7].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['poster'];
                            } else if (!firstTable[7].value) {
                                firstTable[7].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                            }

                            // second table elements go across then down
                            var secondTable = form.find('table:nth-child(4) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['airsDayOfWeek']) {
                                secondTable[0].value = tvdbData['airsDayOfWeek'];
                            }
                            if (tvdbData['airsTime']) {
                                secondTable[1].value = tvdbData['airsTime'];
                            }
                            if (tvdbData['rating']) {
                                secondTable[2].value = tvdbData['rating'];
                            }
                            if (tvmazeData['network'] && tvmazeData['network']['country'] && tvmazeData['network']['country']['code']) {
                                secondTable[3].value = tvmazeData['network']['country']['code'];
                            }
                            if (tvdbData['firstAired']) {
                                secondTable[4].value = tvdbData['firstAired'];
                            }
                            if (tvdbData['network']) {
                                secondTable[5].value = tvdbData['network'];
                            }
                            if (tvdbData['runtime']) {
                                secondTable[6].value = tvdbData['runtime'];
                            }
                            if (tvdbData['siteRating']) {
                                secondTable[7].value = tvdbData['siteRating'];
                            }
                            if (tvmazeData['seasons']) {
                                secondTable[8].value = tvmazeData['seasons'].length;
                            }
                            if (tvdbData['status']) {
                                secondTable[9].value = tvdbData['status'];
                            }
                            if (tvmazeData['type']) {
                                secondTable[10].value = tvmazeData['type'];
                            }
                            secondTable[11].value = tvdbSeriesId;
                            if (tvmazeData['externals'] && tvmazeData['externals']['tvrage']) {
                                secondTable[12].value = tvmazeData['externals']['tvrage'];
                            }
                            return tvdbData['overview'];
                        }).then(function (overview) {
                            if (overview) {
                                alertify.log('Adding series overview for series ID ' + tvdbSeriesId);
                                return Promise.resolve($.post('https://broadcasthe.net/series.php', {
                                    seriesid: seriesId,
                                    action:   'edit_summary',
                                    btn_body: overview
                                }));
                            } else {
                                alertify.error('No overview available on TVDB for series ID ' + tvdbSeriesId);
                                return false;
                            }
                        }).then(function () {
                            alertify.success('Autofill for series ID ' + tvdbSeriesId + ' complete.');
                        }).catch(function (err) {
                            errorHandler('Autofill for series ID ' + tvdbSeriesId + ' failed', err);
                        });

                        return false;
                    }
                }).prependTo(row);
            }

            run(noBannerLinks, 'noBannerLinks');
            run(autofillSeriesPage, 'autofillSeriesPage');
        }

        function modifyTorrentEditPage() {
            var currentOrigin;

            function internalModifier() {
                if (!currentOrigin) {
                    var l = document.getElementById('origin');
                    currentOrigin = l.options[l.selectedIndex].value;
                }
                window.Origin = exportFunction(function () {
                    var l = document.getElementById('origin');
                    var origin = l.options[l.selectedIndex].value;
                    var outputStr = '';
                    if (origin === 'Mixed') {
                        outputStr = 'You can\'t upload a season pack with mixed origin without prior staff consent!';
                    } else if (origin === 'Internal') {
                        outputStr = 'Multiplier set to 2.0x.';
                    }

                    if (origin !== window.currentOrigin && origin === 'Internal') {
                        $('#internal').prop('checked', true);
                        var torrentId = url.match(/(?!torrent)id=([0-9]+)/)[1];
                        $.post('https://broadcasthe.net/torrents.php', {
                            action:    'takemulti',
                            torrentid: torrentId,
                            upload:    2
                        });
                        $('select[name="upload"]').val('2');
                    }
                    document.getElementById('mixedOriginWarning').innerHTML = outputStr;
                    currentOrigin = origin;
                }, window);
            }

            run(internalModifier, 'internalModifier');
            modifyTorrentUploadPage();
        }

        function modifyTorrentUploadPage() {
            function blockContainerChange() {
                // Prevent autofill from changing MP4 -> SD or MKV -> 720p
                window.Format = exportFunction(function () {
                    console.log('Blocked format/resolution auto change');
                }, window);
            }

            function autoTorrentFix() {
                $('#post').before($('<input>', {
                    id:    'autofix',
                    type:  'button',
                    value: 'Auto fix errors',
                    click: function () {
                        try {
                            var containerSelect = document.querySelector('#format');
                            var codecSelect = document.querySelector('#bitrate');
                            var sourceSelect = document.querySelector('#media');
                            var resolutionSelect = document.querySelector('#resolution');
                            var originSelect = document.querySelector('#origin');

                            var log = document.querySelector('#release_desc').value;
                            var mediaInfos = MediaInfo.getAllMediaInfos(log);
                            var mi = mediaInfos[0];

                            var container = mi.getContainer();
                            var videoCodec = mi.getVideoCodec();
                            var audioCodec = mi.getPrimaryAudioCodec();
                            var resolution = mi.getVideoResolution();
                            var origin = originSelect.options[originSelect.selectedIndex].text;
                            var source = '';

                            var releaseNameBox = document.querySelector('#scenename');
                            var releaseName = releaseNameBox.value;

                            var oldData = {};
                            oldData['Release name'] = releaseName;
                            oldData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            oldData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            oldData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            oldData['Origin'] = origin;
                            oldData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            releaseName = releaseName.replace(/^\.*/, '');
                            releaseName = releaseName.replace(/^F1/, 'Formula.1');
                            releaseName = releaseName.replace(/Prelims/i, 'Preliminaries');
                            releaseName = releaseName.replace(/\(|\)|:|SD/g, '.');
                            releaseName = releaseName.replace(/&/g, '.and.');
                            releaseName = releaseName.replace(/BDRip|Bluray/, 'BluRay');
                            releaseName = releaseName.replace(/\.s([0-9]+)e([0-9]+)\./, ".S$1E$2.");
                            releaseName = Latinise.latinise(releaseName);

                            var releaseNameRes = releaseName.match(/\.([0-9]+)(p|i)\./);
                            if (releaseNameRes) {
                                if (resolution == 'SD') {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], '');
                                } else {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], resolution);
                                }
                            }

                            var sourceCasing = {
                                'HDTV':    /hdtv/i,
                                'PDTV':    /pdtv/i,
                                'DSR':     /dsr/i,
                                'DVDRip':  /dvdrip/i,
                                'TVRip':   /tvrip/i,
                                'VHSRip':  /vhsrip/i,
                                'BluRay':  /bluray/i,
                                'BDRip':   /bdrip/i,
                                'DVD5':    /dvd5/i,
                                'DVD9':    /dvd9/i,
                                'WEBRip':  /webrip/i,
                                'WEB-DL':  /web.?dl/i,
                                'WEB':     /web/i,
                                'BD25':    /bd25/i,
                                'BD50':    /bd50/i,
                                'Mixed':   /mixed/i,
                                'Unknown': /unknown/i,
                                'Remux':   /remux/i,
                            };

                            var sourceCases = Object.keys(sourceCasing);
                            for (var i = 0; i < sourceCases.length; i++) {
                                releaseName = releaseName.replace(sourceCasing[sourceCases[i]], sourceCases[i]);
                            }

                            releaseName = releaseName.replace(/\\|'|"/g, '');
                            releaseName = releaseName.replace(/\s|\.-\./g, '.');
                            releaseName = releaseName.replace(/\.{2,}/g, '.');

                            var releaseRegex = /(.*)(HDTV|PDTV|DSR|DVDRip|TVRip|VHSRip|BluRay\.Remux|BluRay|BDRip|WEBRip|WEB-DL|WEB|BD25|BD50|Mixed|Unknown)(?:\.(.*))?\.(.*?(?:-Hi10P)?)(?:[-.]|$)(.*)/i;
                            var releaseMatch = releaseName.match(releaseRegex);

                            var tvTitle = document.querySelector('#title').value;
                            tvTitle = tvTitle.replace(/\\/g, '');
                            document.querySelector('#title').value = tvTitle;

                            // Don't modify scene release names or release names from P2P that are Japanese (likely to be anime) unless in a season pack
                            var shouldModifyReleaseName = ((origin != "Scene" && !(origin == "P2P" && document.querySelector('#country').selectedIndex == 52)) || /^Season [0-9]+$/.test(tvTitle));
                            
                            if (releaseMatch) {
                                source = releaseMatch[2];
                                if (source == 'WEB') {
                                    source = 'WEBRip';
                                } else if (source == 'BDRip') {
                                    source = 'BluRay';
                                }
                                // Check if scene
                                if (shouldModifyReleaseName) {
                                    var group = releaseMatch[5] || 'NOGRP';
                                    var beginningOfReleaseName = releaseMatch[1];
                                    if (resolution != 'SD' && beginningOfReleaseName.indexOf(resolution) == -1) {
                                        beginningOfReleaseName += resolution + '.';
                                    }

                                    releaseName = beginningOfReleaseName + source + '.';

                                    if (audioCodec != 'MP3' && audioCodec != 'MP2') {
                                        releaseName += audioCodec + '.';
                                    }

                                    releaseName += videoCodec + '-' + group;
                                    releaseNameBox.value = releaseName;
                                }
                            }
                            if (resolution == '576p') {
                                resolution = 'SD';
                            }
                            if (videoCodec == 'x264') {
                                videoCodec = 'H.264';
                            } else if (videoCodec == 'DivX') {
                                videoCodec = 'DiVX';
                            } else if (videoCodec == 'Xvid') {
                                videoCodec = 'XViD';
                            } else if (videoCodec == 'x265') {
                                videoCodec = 'H.265';
                            } else if (container == 'VOB') {
                                videoCodec = 'DVD';
                            }

                            if (source == 'BluRay' && container != 'M2TS') {
                                source = 'Bluray';
                            }

                            selectDropdown('#format', container);
                            selectDropdown('#bitrate', videoCodec);
                            selectDropdown('#media', source);
                            selectDropdown('#resolution', resolution);

                            var newData = {};
                            newData['Release name'] = releaseNameBox.value;
                            newData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            newData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            newData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            newData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            var differences = [];
                            var differenceKeys = Object.keys(newData);

                            for (var i = 0; i < differenceKeys.length; i++) {
                                var key = differenceKeys[i];
                                var newValue = newData[key];
                                var oldValue = oldData[key];
                                if (oldValue != newValue) {
                                    if (key == 'Release name') {
                                        var diffTexts = Utils.getHTMLDiff(oldValue, newValue);
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + diffTexts[0] + '</div><br><div class="tab2">' + diffTexts[1] + '</div></div>');
                                    } else {
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + oldData[key] + ' &#8594; ' + newData[key] + '</div></div>');
                                    }
                                }
                            }
                            if (differences.length > 0) {
                                alertify.success('Changed the following items:<br>' + differences.join('<br>'));
                            } else {
                                alertify.error('No changes made.');
                            }
                            return false;
                        } catch (err) {
                            errorHandler('Failed to automatically fix torrent', err);
                            return false;
                        }
                    }
                }));
            }

            function checkReleaseNameLength() {
                var releaseNameBox = $('#scenename');
                var lengthWarning = $('<div>', {
                    style: 'color: red; font-weight: bold'
                });
                lengthWarning.hide();
                releaseNameBox.after(lengthWarning);
                releaseNameBox.on('input', function (e) {
                    if (e.target.value.length > 100) {
                        lengthWarning.text('Warning: Release name is ' + e.target.value.length + ' characters and will be truncated to 100.');
                        lengthWarning.show();
                    } else {
                        lengthWarning.hide();
                    }
                });
            }

            run(checkReleaseNameLength, 'checkReleaseNameLength');
            run(autoTorrentFix, 'autoTorrentFix');
            run(blockContainerChange, 'blockContainerChange');
        }

        function modifyTorrentDetailPage() {
            function autofillTorrentGroupDescription() {
                function generateTorrentDescription(episodeName, season, episode, firstAired, overview, episodeImage) {
                    var desc = '[b]Episode Name:[/b] ' + (episodeName || '');
                    desc += '\n[b]Season:[/b] ' + season;
                    desc += '\n[b]Episode:[/b] ' + episode;
                    desc += '\n[b]Aired:[/b] ' + firstAired || '';
                    desc += '\n\n[b]Episode Overview:[/b] ';
                    if (overview) {
                        desc += '\n' + overview;
                    }
                    if (episodeImage) {
                        desc += '\n\n[b]Episode Image[/b] \n[img=https://cdn2.broadcasthe.net/tvdb/banners/' + episodeImage + ']';
                    }
                    return desc;
                }


                var linkbox = $('div.linkbox')[0];
                $('<a>', {
                    text:  '[Autofill description]',
                    href:  '#',
                    click: function () {
                        if (/^BroadcasTheNet$/.test(document.title)) {
                            alertify.error('Fix this torrent before autofilling!');
                            return false;
                        }
                        var groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                        var episodeName = document.querySelector('#content > div > h2 > a > img').title || '';
                        var episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                        var doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                        var seasonPackNum = episodeName.match(/.*Season ([0-9]+).*/);
                        var episodePromptPromise;
                        if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                            episodePromptPromise = alertify.okBtn('OK')
                                .cancelBtn('Cancel')
                                .defaultValue('S00E01')
                                .prompt('Could not find a season/episode in the episode name. Please provide one manually.')
                                .then(function (resolvedValue) {
                                    resolvedValue.event.preventDefault();
                                    if (resolvedValue.buttonClicked == 'ok') {
                                        episodeName = resolvedValue.inputValue || '';
                                        episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                                        doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                                        seasonPackNum = episodeName.match(/.*Season ([0-9]+).*/);
                                    }
                                    if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                                        throw 'Autofill only supports numbered episodes or season packs.';
                                    }
                                });
                        } else {
                            episodePromptPromise = Promise.resolve();
                        }

                        episodePromptPromise.then(function () {
                            var torrentSeason = -1;
                            var torrentEpisode = -1;
                            var torrentSecondEpisode = -1;
                            if (doubleEpisodeSE) {
                                doubleEpisodeSE = doubleEpisodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = doubleEpisodeSE[1];
                                torrentEpisode = doubleEpisodeSE[2];
                                torrentSecondEpisode = doubleEpisodeSE[3];
                            } else if (seasonPackNum) {
                                seasonPackNum = parseInt(seasonPackNum[1]);
                                torrentSeason = seasonPackNum;
                            } else {
                                episodeSE = episodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = episodeSE[1];
                                torrentEpisode = episodeSE[2];
                            }

                            var loginInfo = tvdbLogin();
                            if (!loginInfo) {
                                return false;
                            }

                            var apikey = loginInfo.apikey;
                            var username = loginInfo.username;
                            var userkey = loginInfo.userkey;

                            var client = new TVDB();

                            var getYearRegex = /([0-9]+)\-[0-9]+\-[0-9]+/;

                            var btnSeriesId = document.querySelector('#content > div > h2 > a').href.split('?id=')[1];
                            var tvdbSeriesId = 0;
                            var languageId = 7;
                            var p = Promise.resolve($.get('https://broadcasthe.net/series.php?action=edit_info&seriesid=' + btnSeriesId))
                                .then(function (data) {
                                    var pageContent = $(data).find('#content');
                                    tvdbSeriesId = parseInt(pageContent
                                        .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                                        .val());
                                    var tvdbSeriesLink = pageContent
                                        .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                                        .val();
                                    if (tvdbSeriesLink) {
                                        var tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                                        if (tvdbLinkMatch) {
                                            tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                            if (tvdbLinkMatch[2]) {
                                                languageId = parseInt(tvdbLinkMatch[2]);
                                            }
                                        }
                                    }

                                    if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                                        throw 'Cannot autofill - series does not have a TVDB ID.';
                                    }

                                    alertify.log('Torrent autofill is currently working...');

                                    return tvdbSeriesId;
                                });

                            if (torrentSecondEpisode !== -1) {
                                // double episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentSecondEpisode)
                                            ]);
                                        }).then(function (data) {
                                            var desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            desc += '\n\n' + generateTorrentDescription(data[2]['episodeName'], torrentSeason, torrentSecondEpisode, data[2]['firstAired'], data[2]['overview'], data[2]['filename']);
                                            var ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else if (torrentEpisode !== -1) {
                                // episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode)
                                            ]);
                                        }).then(function (data) {
                                            var desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            var ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else {
                                // season pack
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesSeason(tvdbSeriesId, torrentSeason)
                                            ]);
                                        }).then(function (data) {
                                            data[1].sort(function (e1, e2) {
                                                return e1['airedEpisodeNumber'] - e2['airedEpisodeNumber'];
                                            });
                                            var desc = data[1].map(function (episode) {
                                                return generateTorrentDescription(episode['episodeName'], episode['airedSeason'], episode['airedEpisodeNumber'], episode['firstAired'], episode['overview'], episode['filename']);
                                            }).reduce(function (previousValue, currentValue) {
                                                return previousValue + currentValue + '\n\n';
                                            }, '');
                                            var ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1][0]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1][0]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            }
                            return p.then(function (data) {
                                return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data[0]))
                                    .then(function () {
                                        return data[1];
                                    });
                            }).then(function (data) {
                                if (data) {
                                    return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data));
                                } else {
                                    alertify.error('Could not autofill aired year since it was unavailable on TVDB.');
                                    return true;
                                }
                            }).then(function () {
                                return Promise.resolve($.get(url));
                            }).then(function (data) {
                                var dom = $(data);
                                $('div.body:eq(1)').replaceWith(dom.find('div.body:eq(1)'));
                                $('#content > div > h2').replaceWith(dom.find('#content > div > h2'));
                                alertify.success('Torrent autofill complete - changes are now visible.');
                            });
                        }).catch(function (err) {
                            errorHandler('Autofill for torrent group ' + groupId + ' failed', err);
                        });
                        return false;
                    }
                }).prependTo(linkbox);
            }

            run(autofillTorrentGroupDescription, 'autofillTorrentGroup');

            $('tr.pad > td > div.linkbox').each(function () {
                // Hide obnoxious file list
                var $this = $(this);
                var filelist = $this.next().next();
                $('<a>', {
                    text:  '[File List]',
                    href:  '#',
                    click: function () {
                        filelist.toggle();
                        return false;
                    }
                }).prependTo($this);

                // Count number of files in torrents
                var filenum = filelist.find('tbody').children().length - 1;
                var output = ' (' + filenum + ' file';
                if (filenum > 1) {
                    output += 's';
                }
                output += ')';
                $this.parent().parent().prev().prev().children()[1].innerHTML += output;

                if (filenum > 50) {
                    filelist.hide();
                }
            });

            // Add search links for torrents
            $('table.torrent_table > tbody > tr > td:nth-child(1) > a').each(function () {
                var releaseNode = this.parentNode.parentNode.nextSibling.nextSibling.children[0];
                var relQueryObj = $(releaseNode.children[0]);
                run(function () {
                    relQueryObj.append($('<br>'));
                }, 'torrentSearchesNewLine');

                // Only add log button if this user has log access
                if ($('input[placeholder=\'Log\']').length > 0) {
                    run(function (that) {
                        var linkbox = that.previousSibling.previousSibling.children;
                        var reqLink = linkbox[linkbox.length - 1].href.split('=');
                        var torrentId = reqLink[reqLink.length - 1];
                        relQueryObj.append($('<a>', {
                            href: 'https://broadcasthe.net/log.php?search=' + torrentId,
                            text: '[Log] '
                        }));
                    }, 'searchLog', this);
                }

                if (this.innerHTML.endsWith('Scene')) {
                    var releaseName = releaseNode.childNodes[0].nodeValue.replace(/\s|»/g, '');

                    var providerNames = Object.keys(searchProviders);

                    for (var i = 0; i < providerNames.length; i++) {
                        var searchName = providerNames[i];
                        var searchUrl = searchProviders[searchName];
                        run(function (searchUrl, searchName, releaseName) {
                            var newSearchUrl = searchUrl.replace(/__RELEASE_NAME__/, releaseName);
                            newSearchUrl = newSearchUrl.replace(/__ENCODED_RELEASE_NAME__/, encodeURIComponent(releaseName));
                            relQueryObj.append($('<a>', {
                                href: newSearchUrl,
                                text: '[' + searchName + '] '
                            }));
                        }, 'search' + searchName, searchUrl, searchName, releaseName);
                    }
                }
            });

            // Add reseed button to all torrents
            run(function () {
                $('td > blockquote').prev().filter(function () {
                    var prevId = $(this).attr('id');
                    return prevId && prevId.indexOf('subtitle') > -1;
                }).each(function () {
                    var $this = $(this);
                    var torrentId = $this.attr('id').match('subtitle_dialog_([0-9]+)')[1];
                    var groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                    $this.after($('<div id="linkbox" style="text-align: center"><form method="post" action=""><input type="hidden" id="action" name="action" value="reseed"><input type="hidden" id="gid" name="gid" value="' + groupId + '"><input type="hidden" id="tid" name="tid" value="' + torrentId + '"><input type="submit" id="reseed" name="reseed" value="Request a reseed"></form></div>'));
                });
            }, 'addReseedButton');

            // Copy release link to clipboard
            $('a[title="RequestLink"]').on('click', function(event) {
                event.stopPropagation();
                copyToClipboard(this.href);
                return false;
            });
        }

        function addPMToUsernames() {
            $('#user .box.pad.center tbody tr > td a,' +
                '#torrents .torrent_table table ~ div + blockquote > a,' +
                '#torrents .torrent_table table ~ table + blockquote > a,' +
                '#torrents .torrent_table .torrent > td:nth-child(3) > .nobr > a')
                .each(function () {
                    var $this = $(this);
                    var id = $this.attr('href').replace('user.php?id=', '');
                    $this.after(' <a href="inbox.php?action=compose&to=' + id + '">[PM]</a> ');
                });
        }

        function modifySnatchlistPage() {
            function snatchListFilter() {
                // pulled from https://github.com/Userscriptz/BTN-Snatchlist/blob/master/main.user.js

                function changeTable() {
                    var pref = Preferences.getInstance('Snatchlist');
                    var snatchView = pref.getItem('selectedmode', 'all');
                    var links = pref.getItem('dllinks', 'no');

                    if (links == 'yes') {
                        $('.download-btn').show();
                    } else {
                        $('.download-btn').hide();
                    }

                    $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                        if ($(this).attr('class') != 'colhead_dark') {
                            var id = '#' + $(this).attr('id');
                            var status = $(id + ' > td:nth-child(7) > span').text();
                            var seedTimeLeft = $(id + ' > td:nth-child(5)').text();
                            var remove;

                            if (snatchView == 'showcomplete') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Seeding');
                            }
                            else if (snatchView == 'showcomplete2') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Complete');
                            }
                            else if (snatchView == 'seedtimeleft') {
                                remove = (seedTimeLeft == 'Complete' || status == 'Complete');
                            }
                            else if (snatchView == 'inactive') {
                                remove = (status != 'Inactive');
                            }
                            else if (snatchView == 'leeching') {
                                remove = (status != 'Leeching');
                            }
                            else {
                                remove = false;
                            }

                            if (remove) {
                                $(id).hide();
                            } else {
                                $(id).show();
                            }
                        }
                    });
                }

                var pref = Preferences.getInstance('Snatchlist');
                var selectedMode = pref.getItem('selectedmode', 'all');
                var dllinks = pref.getItem('dllinks', 'no');

                var cssStyle = '.mode-active {color:limegreen; font-weight:bold;} ';
                cssStyle += '#div-1 {position:relative; padding-top:40px;} ';
                cssStyle += '#div-1a {position:absolute; top:0; right:0; width:170px;} ';
                cssStyle += '#div-1b {position:absolute; top:0; left:0; width:600px;} ';
                cssStyle += '.mode-hidden {display: none} ';
                $("<style>")
                    .prop("type", "text/css")
                    .html(cssStyle)
                    .appendTo("head");

                var selectModeLink = function (text, key, mode, active) {
                    return $('<a>', {
                        text:  text,
                        href:  '#',
                        click: function (e) {
                            pref.setItem(key, mode);
                            $('a').filter('.mode-active.' + key).removeClass('mode-active');
                            $(this).addClass('mode-active');
                            changeTable();
                            e.preventDefault();
                        },
                        class: (active ? 'mode-active ' : '') + key
                    });
                };

                var div1b = $('<div>', {
                    id:   'div-1b',
                    text: 'Torrents: '
                });

                var div1a = $('<div>', {
                    id:   'div-1a',
                    text: 'Downloads: '
                });

                div1b.append(selectModeLink('All', 'selectedmode', 'all', selectedMode == 'all'), ' | ',
                    selectModeLink('Inactive', 'selectedmode', 'inactive', selectedMode == 'inactive'), ' | ',
                    selectModeLink('Leeching', 'selectedmode', 'leeching', selectedMode == 'leeching'), ' | ',
                    selectModeLink('Seed Time Left', 'selectedmode', 'seedtimeleft', selectedMode == 'seedtimeleft'), ' | ',
                    selectModeLink('Complete (Seeding)', 'selectedmode', 'showcomplete', selectedMode == 'showcomplete'), ' | ',
                    selectModeLink('Complete', 'selectedmode', 'showcomplete2', selectedMode == 'showcomplete2'));

                div1a.append(selectModeLink('Enable', 'dllinks', 'yes', dllinks == 'yes'), ' | ',
                    selectModeLink('Disable', 'dllinks', 'no', dllinks == 'no'));

                var div1 = $('<div>', {
                    id: 'div-1'
                });
                div1.append(div1b, div1a);
                div1.prependTo('#content');

                var modifySnatchlistElements = function () {
                    var authkey = pref.getItem('authkey');
                    var torrent_pass = pref.getItem('torrent_pass');
                    if (dllinks == 'yes' && (!authkey || !torrent_pass)) {
                        alertify.error('<div><h3>Need torrent passkey</h3><p>To generate torrent download links, your passkey is needed. It can be entered in the Options menu.</p><p>Optionally, you can also disable torrent download links to hide this notice.</p></div>');
                    } else {
                        $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                            if ($(this).attr('class') != 'colhead_dark') {
                                var id = '#' + $(this).attr('id');
                                var torrentid = $(id + ' > td:nth-child(1) > span > a:nth-child(2)')
                                    .attr('href')
                                    .match(/torrentid=([0-9]+)/)[1];
                                var link = $('<a>', {
                                    href:  'http://broadcasthe.net/torrents.php?action=download&id=' + torrentid + '&authkey=' + authkey + '&torrent_pass=' + torrent_pass,
                                    class: 'download-btn'
                                });
                                $('<img>', {
                                    src: 'https://cdn.broadcasthe.net/common/browsetags/download.png'
                                }).appendTo(link);
                                link.appendTo(id + ' > td:nth-child(1) > span');
                            }
                        });
                    }
                    changeTable();
                };

                window.SortSnatch = exportFunction(function (sort, uid, page) {
                    var old = '';
                    var hnr = '';
                    if (window.location.href.search("old") !== -1) {
                        old = '&old=1';
                    }
                    if (window.location.href.search("hnr") !== -1) {
                        hnr = '&hnr=1';
                    }
                    console.log('Fetching page ' + page);
                    Promise.resolve($.get("https://broadcasthe.net/snatchlist.php?type=ajax&id=" + uid + "&sort=" + sort + "&page=" + page + old + hnr))
                        .then(function (data) {
                            data = $(data)[0];
                            console.log('Fetched page ' + page);
                            $('#SnatchData').replaceWith(data);
                            modifySnatchlistElements();
                        });
                    return false;
                }, window);

                modifySnatchlistElements();
            }

            snatchListFilter();
        }

        function modifyTorrentListPage() {

            run(function () {
                $('#torrent_table').find('tbody > tr > td:nth-child(3)').slice(1).each(function () {
                    var $this = $(this);

                    // cleanup some crap
                    $this.find('br').remove();
                    $this.children('img').remove();
                    var contents = $this.contents();
                    if ($this.children('a')[0].title == 'View Series') {
                        contents.slice(3, 6).wrapAll('<div class="nobr">');
                    } else {
                        $this.children('a').slice(0, 1).wrapAll('<div class="nobr">');
                    }

                    var torrentId = $this.find('a[title="View Torrent"]')
                        .attr('href')
                        .match(/torrents\.php\?id=[0-9]+&torrentid=([0-9]+)/)[1];

                    contents = $this.contents();

                    // get rid of <strong> tag wrapping "Internal" if it exists
                    var torrentSpecsNode = contents.slice(5, 6);
                    var nextNode = torrentSpecsNode[0].nextSibling;
                    var torrentSpecsText = torrentSpecsNode[0].nodeValue;
                    while (nextNode.localName != 'div') {
                        torrentSpecsText += nextNode.innerHTML || nextNode.nodeValue;
                        nextNode = nextNode.nextSibling;
                        $(nextNode.previousSibling).remove();
                    }

                    // color code format, codec, source, resolution, origin, year
                    var torrentSpecs = torrentSpecsText.match(/\[([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+)]\s*\[([0-9]+)]/);
                    var newTorrentSpecsNode = '<div class="nobr">[';
                    for (var i = 1; i < 5; i++) {
                        newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[i] + '">' + torrentSpecs[i];
                        newTorrentSpecsNode += '</div> / ';
                    }
                    newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[5] + '">' + torrentSpecs[5];
                    newTorrentSpecsNode += '</div>] [<div class="inline year">' + torrentSpecs[6] + '</div>] [<a href="https://broadcasthe.net/torrents.php?action=edit&id=' + torrentId + '">Edit</a>]</div>';
                    torrentSpecsNode.replaceWith(newTorrentSpecsNode);
                });
            }, 'colorCodeTorrents');
            run(function () {
                // Disabled temporarily
                if ($('#torrent_table').width() > 1000 && false) {
                    $('#torrent_table').find('tbody > tr > td:nth-child(3) > div > span').each(function () {
                        var that = $(this);
                        that.text(' ' + that.attr('title'));
                    });
                } else {
                    $('#torrent_table').find('tbody > tr > td:nth-child(3) > div > span').mouseover(function () {
                        var that = $(this);
                        var newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    }).mouseout(function () {
                        var that = $(this);
                        var newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    });
                }
            }, 'expandReleaseNames');
            $('.watched').mouseover(function () {
                var that = $(this);
                if (that.attr('title').length < 50) {
                    that.text(' ' + that.attr('title'));
                }
            }).mouseout(function () {
                $(this).text(' [!]');
            });
        }

        function modifyUserDetailPage() {
            var userId = url.match(/user.php\?id=([0-9]+)/)[1];
            run(function () {
                $('#section2')
                    .find('li:nth-child(14)')
                    .after('<li><a href="https://broadcasthe.net/snatchlist.php?id=' + userId + '">Snatchlist</a></li>');
            }, 'addSnatchlistLink');
            run(function () {
                $('#section3 > div > div.pad.center > img').remove();
                $('#section3 > div > div.pad.center > a').remove();
            }, 'blockUserStamps');
            stampObserver.disconnect();
        }

        function modifyUserProfileOptionsPage() {
            run(function () {
                $('#torpersnatch')
                    .replaceWith('<input name="torpersnatch" id="torpersnatch" type="number" min="10" max="100000" step="10" value="1000" size="6">');
                $('#torperpage')
                    .replaceWith('<input name="torperpage" id="torperpage" type="number" min="25" max="100000" step="5" value="100" size="6">');
            }, 'modifyUserProfileOptionsPage');
        }

        addPolyfillsAndStyles();
        addOptionsToMenu();

        run(addPMToUsernames, 'addPMToUsernames');

        if (/^user\.php\?action=edit&userid=[0-9]+/.test(url)) {
            modifyUserProfileOptionsPage();
        }
        if (/^torrents.php\?(?:page=[0-9]+&)?(?:(?:id=[0-9]+)|(?:torrentid=[0-9]+))/.test(url)) {
            modifyTorrentDetailPage();
        }
        if (/^torrents.php\?action=edit&id=[0-9]+/.test(url)) {
            modifyTorrentEditPage();
        }
        if (/^upload.php/.test(url)) {
            modifyTorrentUploadPage();
        }
        if (/^series.php\?action=edit_info/.test(url)) {
            modifySeriesEditPage();
        }
        if (/^snatchlist.php/.test(url)) {
            modifySnatchlistPage();
        }
        if (/^torrents.php.*(?!id)/.test(url)) {
            modifyTorrentListPage();
        }
        if (/^user.php\?id=[0-9]+/.test(url)) {
            modifyUserDetailPage();
        }
        if (/^forums.php/.test(url)) {
            modifyForumPage();
        }
    });
}($));

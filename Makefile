all: stafftools.user.js nerfed-tools.user.js css
	cat stafftools.user.js | pbcopy

stafftools.user.js: MediaInfo.js TVMaze.js TVDBv2.js Preferences.js stafftools.dev.js latinise.js package.json header.txt
	browserify stafftools.dev.js -o stafftools2.js
	cat header.txt stafftools2.js > stafftools.user.js
	rm stafftools2.js

nerfed-tools.user.js: MediaInfo.js TVMaze.js TVDBv2.js Preferences.js nerfed-tools.dev.js latinise.js package.json header.txt
	browserify nerfed-tools.dev.js -o stafftools2.js
	cat nerfed-header.txt stafftools2.js > nerfed-tools.user.js
	rm stafftools2.js

css: css/styles.css

css/styles.css: less/styles.less
	lessc less/styles.less css/styles.css

serve:
	node dev-server.js

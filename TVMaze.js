'use strict';
var Promise = require('bluebird');

function GM_XHR() {
    this.type = null;
    this.url = null;
    this.async = null;
    this.username = null;
    this.password = null;
    this.status = null;
    this.headers = {};
    this.readyState = null;

    this.abort = function () {
        this.readyState = 0;
    };

    this.getAllResponseHeaders = function (name) {
        if (this.readyState != 4) {
            return "";
        }
        return this.responseHeaders;
    };

    this.getResponseHeader = function (name) {
        var regexp = new RegExp('^' + name + ': (.*)$', 'im');
        var match = regexp.exec(this.responseHeaders);
        if (match) {
            return match[1];
        }
        return '';
    };

    this.open = function (type, url, async, username, password) {
        this.type = type ? type : null;
        this.url = url ? url : null;
        this.async = async ? async : null;
        this.username = username ? username : null;
        this.password = password ? password : null;
        this.readyState = 1;
    };

    this.setRequestHeader = function (name, value) {
        this.headers[name] = value;
    };

    this.send = function (data) {
        this.data = data;
        var that = this;
        // http://wiki.greasespot.net/GM_xmlhttpRequest
        GM_xmlhttpRequest({
            method:  this.type,
            url:     this.url,
            headers: this.headers,
            data:    this.data,
            onload:  function (rsp) {
                // Populate wrapper object with returned data
                // including the Greasemonkey specific "responseHeaders"
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                // now we call onreadystatechange
                if (that.onload) {
                    that.onload();
                } else {
                    that.onreadystatechange();
                }
            },
            onerror: function (rsp) {
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                if (that.onerror) {
                    that.onerror();
                } else {
                    that.onreadystatechange();
                }
            }
        });
    };
}

class TVMaze {

    getSeriesByTVDBId(tvdbSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?thetvdb=' + tvdbSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeriesByIMDBId(imdbSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?imdb=' + imdbSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeriesByTVRageId(tvrageSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?tvrage=' + tvrageSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeries(seriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeasons(seriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/seasons',
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisode(seriesId, season, episode) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/episodebynumber?season=' + season + '&number=' + episode,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisodes(seriesId, specials) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/episodes?specials=' + (!!specials ? 1 : 0),
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisodesWithSpecials(seriesId) {
        return getEpisodes(seriesId, true);
    }
}

module.exports = TVMaze;

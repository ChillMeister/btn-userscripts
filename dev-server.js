var http = require('http');
var express = require('express');
var path = require('path');

var app = express();
app.use(require('morgan')('combined'));
app.use(express.static('.'));

var server = http.createServer(app);

server.listen(8080);

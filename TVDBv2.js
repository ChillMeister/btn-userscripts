'use strict';
var Promise = require('bluebird');

function GM_XHR() {
    this.type = null;
    this.url = null;
    this.async = null;
    this.username = null;
    this.password = null;
    this.status = null;
    this.headers = {};
    this.readyState = null;

    this.abort = function () {
        this.readyState = 0;
    };

    this.getAllResponseHeaders = function (name) {
        if (this.readyState != 4) {
            return "";
        }
        return this.responseHeaders;
    };

    this.getResponseHeader = function (name) {
        var regexp = new RegExp('^' + name + ': (.*)$', 'im');
        var match = regexp.exec(this.responseHeaders);
        if (match) {
            return match[1];
        }
        return '';
    };

    this.open = function (type, url, async, username, password) {
        this.type = type ? type : null;
        this.url = url ? url : null;
        this.async = async ? async : null;
        this.username = username ? username : null;
        this.password = password ? password : null;
        this.readyState = 1;
    };

    this.setRequestHeader = function (name, value) {
        this.headers[name] = value;
    };

    this.send = function (data) {
        this.data = data;
        var that = this;
        // http://wiki.greasespot.net/GM_xmlhttpRequest
        GM_xmlhttpRequest({
            method:  this.type,
            url:     this.url,
            headers: this.headers,
            data:    this.data,
            onload:  function (rsp) {
                // Populate wrapper object with returned data
                // including the Greasemonkey specific "responseHeaders"
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                // now we call onreadystatechange
                if (that.onload) {
                    that.onload();
                } else {
                    that.onreadystatechange();
                }
            },
            onerror: function (rsp) {
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                if (that.onerror) {
                    that.onerror();
                } else {
                    that.onreadystatechange();
                }
            }
        });
    };
}

class TVDBv2 {

    constructor() {
        this.token = '';
        this.languages = {};
    }

    refreshToken() {
        var that = this;
        return Promise.resolve($.ajax({
            url:        'https://api.thetvdb.com/refresh_token',
            xhr:        function () {
                return new GM_XHR();
            },
            type:       'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
            }
        })).then(function (data) {
            that.token = data.token;
            return data.token;
        });
    }

    login(apikey, username, userkey) {
        var that = this;
        return Promise.resolve($.ajax({
            url:        'https://api.thetvdb.com/login',
            xhr:        function () {
                return new GM_XHR();
            },
            type:       'POST',
            data:       JSON.stringify({
                apikey:   apikey,
                username: username,
                userkey:  userkey
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Accept', 'application/json');
            }
        })).then(function (data) {
            that.token = data.token;
            console.log('JWT token: ' + that.token);
            return true;
        }).then(function () {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/languages',
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                }
            })).then(function (data) {
                that.languages = {};
                data = data.data;
                for(var i = 0; i < data.length; i++) {
                    that.languages[data[i]['id']] = data[i]['abbreviation'];
                }
                return true;
            });
        });
    }

    getSeries(seriesId, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            }));
        }
        var englishLanguage = f(7);
        if(languageId && languageId != 7) {
            return Promise.all([englishLanguage, f(languageId)]).then(function(data) {
                var english = data[0].data;
                var otherLang = data[1].data;
                var keys = Object.keys(otherLang);
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    if (otherLang[key] && !english[key]) {
                        english[key] = otherLang[key];
                    }
                }
                if(!english['seriesName']) {
                    throw 'Needs new language ID';
                }
                return english;
            })
        } else {
            return englishLanguage.then(function (data) {
                if(!data.data['seriesName']) {
                    throw 'Needs new language ID';
                }
                return data.data;
            });
        }
    }

    getSeriesEpisode(seriesId, season, episode, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId + '/episodes/query?airedSeason=' + season + '&airedEpisode=' + episode,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return that.getEpisodeById(data.data[0].id);
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesSeason(seriesId, season, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId + '/episodes/query?airedSeason=' + season,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                if (data.data.length > 100) {
                    throw 'Use site autofill for season packs with >100 episodes.'
                }
                var episodes = data.data.map(function (d) {
                    return that.getEpisodeById(d.id);
                });
                return Promise.all(episodes);
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesImages(seriesId, keyType, resolution, subKey, languageId) {
        var that = this;
        var url = 'https://api.thetvdb.com/series/' + seriesId + '/images/query?keyType=' + keyType;
        if (resolution) {
            url += '&resolution=' + resolution;
        }
        if (subKey) {
            url += '&subKey=' + subKey;
        }

        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        url,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return data.data;
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesPoster(seriesId, defaultValue, languageId) {
        var that = this;
        return that.getSeriesImages(seriesId, 'poster', undefined, undefined, languageId)
            .then(function (data) {
                return 'http://thetvdb.com/banners/' + data[0].fileName;
            }).catch(function (err) {
                console.log(err);
                return defaultValue;
            });
    }

    getSeasonPoster(seriesId, season, defaultValue, languageId) {
        var that = this;
        return that.getSeriesImages(seriesId, 'season', undefined, season, languageId)
            .then(function (data) {
                return 'http://thetvdb.com/banners/' + data[0].fileName;
            }).catch(function (err) {
                console.log(err);
                return that.getSeriesPoster(seriesId, defaultValue);
            });
    }

    getEpisodeById(episodeId, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/episodes/' + episodeId,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return data.data;
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }
}

module.exports = TVDBv2;

'use strict';
class Utils {
    static getLongestCommonSubsequence(a, b) {
        var m = a.length, n = b.length,
            C = [], i, j;
        for (i = 0; i <= m; i++) C.push([0]);
        for (j = 0; j < n; j++) C[0].push(0);
        for (i = 0; i < m; i++)
            for (j = 0; j < n; j++)
                C[i+1][j+1] = a[i] === b[j] ? C[i][j]+1 : Math.max(C[i+1][j], C[i][j+1]);
        return (function bt(i, j) {
            if (i*j === 0) { return ""; }
            if (a[i-1] === b[j-1]) { return bt(i-1, j-1) + a[i-1]; }
            return (C[i][j-1] > C[i-1][j]) ? bt(i, j-1) : bt(i-1, j);
        }(m, n));
    }

    static getHTMLDiff(original, modified) {
        var lcs = this.getLongestCommonSubsequence(original, modified);
        var originalOutput = '';
        var modifiedOutput = '';
        var i = 0;
        var j = 0;
        var k = 0;
        var addedBuffer = '';
        var deletedBuffer = '';
        while(i < lcs.length) {
            addedBuffer = '';
            deletedBuffer = '';
            var commonChar = lcs.charAt(i++);
            while(k < modified.length) {
                var modifiedChar = modified.charAt(k++);
                if(modifiedChar == commonChar) {
                    break;
                }
                addedBuffer += modifiedChar;
            }
            if(addedBuffer.length > 0) {
                modifiedOutput += '<div class="str-added">' + addedBuffer + '</div>';
            }
            while(j < original.length) {
                var originalChar = original.charAt(j++);
                if(originalChar == commonChar) {
                    break;
                }
                deletedBuffer += originalChar;
            }
            if(deletedBuffer.length > 0) {
                originalOutput += '<div class="str-removed">' + deletedBuffer + '</div>';
            }
            originalOutput += commonChar;
            modifiedOutput += commonChar;
        }
        addedBuffer = '';
        deletedBuffer = '';
        while(k < modified.length) {
            addedBuffer += modified.charAt(k++);
        }
        if(addedBuffer.length > 0) {
            modifiedOutput += '<div class="str-added">' + addedBuffer + '</div>';
        }
        while(j < original.length) {
            deletedBuffer += original.charAt(j++);
        }
        if(deletedBuffer.length > 0) {
            originalOutput += '<div class="str-removed">' + deletedBuffer + '</div>';
        }
        return [originalOutput, modifiedOutput];
    }

    static escapeHTML(str) {
        var tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        return str.replace(/[&<>]/g, function(tag) {
            return tagsToReplace[tag] || tag;
        });
    }

    static cleanReleaseName(str) {
        return str.replace(/\s/g, '').replace(String.fromCharCode(187), '');
    }
}

module.exports = Utils;
